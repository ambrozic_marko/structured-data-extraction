# structured-data-extraction
This project is written in python 3.7

Required packages:

 - re
 - json
 - lxml
 
The project can be run by simply executing files regexImplementation.py 
or xpathImplementation.py in the implementation directory using python.
