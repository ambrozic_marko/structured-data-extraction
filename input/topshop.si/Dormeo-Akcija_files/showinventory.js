function getSaleable() {
    if (!(jQuery('.showinventoryonproduct').length > 0)){
        return false;
    }
    var list = [];
    //jQuery(".category-products .item .js-gtm-data, .widget-products-carousel .item .js-gtm-data, .widget-products-category-list .item .js-gtm-data, .campaign-products-list-box .li-content .js-gtm-data, #recently-viewed-items-wide-carousel .item .js-gtm-data, #nav-dropdown-container-inner .item .js-gtm-data").each(function (k, v) {
    jQuery(".item .js-gtm-data, .li-content .js-gtm-data").each(function (k, v) {
        var base = jQuery(this);
        var productid = base.data('productid')
        if(typeof productid !== 'undefined') {
            list.push(productid);
        }
    });
    if(list.length < 1){
        return false;
    }
    jQuery.ajax(
        {
            url: "/saleable/saleable/list",
            method: "POST",
            data: {productIds: list}})
        .done(function (saleable) {
            var minq = saleable.minq;
            if (saleable.list == null) {
                return false;
            }
            jQuery.each(saleable.list, function (k, v) {
				
                var pcshide = true;
                var oushide = true;
                if (v !== null) {
                    if ((v.show_inventory) && (v.is_saleable)) {
                        if (v.backorders < 1) {
                            if ((v.quantity) > 0 && (v.quantity < minq)) {

                                var str = jQuery(".pcs" + k).first().text().replace('-','').replace(/\d+/g, v.quantity);
                                jQuery(".pcs" + k).show().text(str);
                                pcshide = false;
                            }
                        }
                    }
                    if (!v.is_saleable) {
                        jQuery(".ous" + k).show();
                        var oushide = false;
                    }
                }
                if (oushide) {
                    jQuery(".ous" + k).remove();
                }
				else
				{
					jQuery(".ous" + k).closest(".item").addClass("out-of-stock-item-active");
					jQuery(".ous" + k).closest(".li-content").addClass("out-of-stock-licontent-active");
					}
                if (pcshide) {
                    jQuery(".pcs" + k).remove();
                }
            });
        })
        .fail(function () {
        })
        .always(function () {
        });
}
jQuery(function () {			 		 
    jQuery(".txt-out-of-stock").hide();
    jQuery(".txt-only-10-left").hide();
    getSaleable();
});