
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"152",
  "macros":[{
      "function":"__aev",
      "vtp_varType":"ELEMENT"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=",["escape",["macro",0],8,16],";a\u0026\u0026a!==document.body\u0026\u0026\"onepage-guest-register-button\"!==a.id;)a=a.parentElement;return\"onepage-guest-register-button\"===a.id})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=jQuery(event.target).closest(\"button\").attr(\"class\");return void 0!=a?-1==a.indexOf(\"cart2_userlogin\")?!1:!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=jQuery(event.target).closest(\"button\").attr(\"class\");return void 0!=a?-1==a.indexOf(\"cart3_payment_options\")?!1:!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 0\u003C$j(\".catalog-category-view\").length?!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 0\u003C$j(\".catalog-product-view, .redboxdigital-quickview-index-index, .cms-imemory-s\").length?!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=jQuery(\"input:radio[name\\x3dcheckout_method]:checked\").val();return\"\/cart3-UserForm-\"+a+\"\/\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 0\u003C$j(\".cart-empty\").length?!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 0\u003C$j(\".black-friday-teaser\").length?!0:!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 0\u003C$j(\".checkout-cart-index\").length?!0:!1})();"]
    },{
      "function":"__v",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":1,
      "vtp_defaultValue":"NoValue",
      "vtp_name":"PageType"
    },{
      "function":"__e"
    },{
      "function":"__c",
      "vtp_value":"UA-1883850-85"
    },{
      "function":"__e"
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__c",
      "vtp_value":"0"
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventValue"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventCategory"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventAction"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventLabel"
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"google_tag_params"
    },{
      "function":"__u",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"external",
      "vtp_name":"IpType"
    },{
      "function":"__c",
      "vtp_value":"UA-11642161-50"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"CrossSellSplitSetValue"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"virtualPageURL"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"virtualPageTitle"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",22],
      "vtp_map":["list",["map","key","www.delimano.si","value","Delimano"],["map","key","www.dormeo.net","value","Dormeo"],["map","key","www.liveactive.si","value","Liveactive"],["map","key","www.topshop.si","value","Topshop"],["map","key","www.walkmaxx.si","value","Walkmaxx"],["map","key","www2.delimano.si","value","Delimano"],["map","key","www2.dormeo.net","value","Dormeo"],["map","key","www2.liveactive.si","value","Liveactive"],["map","key","www2.topshop.si","value","Topshop"],["map","key","www2.walkmaxx.si","value","Walkmaxx"],["map","key","www.wellneo.si","value","Wellneo"],["map","key","www2.wellneo.si","value","Wellneo"],["map","key","www2.superstore.si","value","Superstore"],["map","key","www.superstore.si","value","Superstore"],["map","key","prelive.superstore.si","value","Superstore"],["map","key","si.superstore.ox","value","Superstore"],["map","key","www.rovus.si","value","Rovus"],["map","key","prelive.rovus.si","value","Rovus"],["map","key","www2.rovus.si","value","Rovus"],["map","key","dev.rovus.si","value","Rovus"]]
    },{
      "function":"__c",
      "vtp_value":"652742274804180"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",22],
      "vtp_map":["list",["map","key","www.dormeo.net","value","D9A9A5D70D614A5E8BC1949FB9FD6DC3"],["map","key","www.delimano.si","value","BAF4AA450AF94C6198D7C62324A3E4BE"],["map","key","www.liveactive.si","value","A5017B51EDF747308F5B8DBAE7F9A38A"],["map","key","www.topshop.si","value","7127FD88828446228FEF46AC80A3CEA8"],["map","key","www.walkmaxx.si","value","FDA946A6F85C4D1B93BB60B55832A1BA"],["map","key","www.wellneo.si","value","A5017B51EDF747308F5B8DBAE7F9A38A"]]
    },{
      "function":"__c",
      "vtp_value":"EUR"
    },{
      "function":"__u",
      "vtp_component":"PROTOCOL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",34],
      "vtp_map":["list",["map","key","https","value","https:\/\/www.sc.pages04.net\/lp\/static\/js\/iMAWebCookie.js?211311fe-13cb5096257-6b01afd6fbb6724a9fe9ac1747b0e3f6\u0026h=www.pages04.net"],["map","key","http","value","http:\/\/contentz.mkt941.com\/lp\/static\/js\/iMAWebCookie.js?211311fe-13cb5096257-6b01afd6fbb6724a9fe9ac1747b0e3f6\u0026h=www.pages04.net"]]
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"crsssptst"
    },{
      "function":"__r"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",17],
      "vtp_defaultValue":"",
      "vtp_map":["list",["map","key","\/checkout\/onepage\/success\/","value","1"]]
    },{
      "function":"__v",
      "vtp_dataLayerVersion":1,
      "vtp_name":"CartStep"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":1,
      "vtp_name":"QId"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",22],
      "vtp_map":["list",["map","key","www.topshop.si","value","IH4cNor7QXX02kFpNCSe"],["map","key","www2.topshop.si","value","IH4cNor7QXX02kFpNCSe"],["map","key","prelive.topshop.si","value","IH4cNor7QXX02kFpNCSe"],["map","key","www.dormeo.net","value","bidlmNFmrlAJYp0lJP5v"],["map","key","www2.dormeo.net","value","bidlmNFmrlAJYp0lJP5v"],["map","key","prelive.dormeo.net","value","bidlmNFmrlAJYp0lJP5v"]]
    },{
      "function":"__c",
      "vtp_value":"rtbhouse"
    },{
      "function":"__v",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":1,
      "vtp_defaultValue":"",
      "vtp_name":"transactionId"
    },{
      "function":"__aev",
      "vtp_varType":"CLASSES"
    },{
      "function":"__aev",
      "vtp_varType":"ID"
    },{
      "function":"__aev",
      "vtp_varType":"TARGET"
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__aev",
      "vtp_varType":"URL"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_name":"eventAction"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_name":"eventLabel"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_name":"gtm.errorLineNumber"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_name":"gtm.errorMessage"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_name":"gtm.errorUrl"
    },{
      "function":"__aev",
      "vtp_varType":"HISTORY_CHANGE_SOURCE"
    },{
      "function":"__aev",
      "vtp_varType":"HISTORY_NEW_STATE"
    },{
      "function":"__aev",
      "vtp_varType":"HISTORY_NEW_URL_FRAGMENT"
    },{
      "function":"__aev",
      "vtp_varType":"HISTORY_OLD_STATE"
    },{
      "function":"__aev",
      "vtp_varType":"HISTORY_OLD_URL_FRAGMENT"
    },{
      "function":"__f"
    },{
      "function":"__c",
      "vtp_value":"879085990"
    },{
      "function":"__c",
      "vtp_value":"879088195"
    },{
      "function":"__c",
      "vtp_value":"879086062"
    },{
      "function":"__c",
      "vtp_value":"879095593"
    },{
      "function":"__c",
      "vtp_value":"879090870"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",22],
      "vtp_map":["list",["map","key","www.delimano.si","value","1082"],["map","key","www.dormeo.net","value","1081"],["map","key","www.liveactive.si","value","1083"],["map","key","www.topshop.si","value","1080"],["map","key","www.walkmaxx.si","value","1084"],["map","key","www2.delimano.si","value","1082"],["map","key","www2.dormeo.net","value","1081"],["map","key","www2.liveactive.si","value","1083"],["map","key","www2.topshop.si","value","1080"],["map","key","www2.walkmaxx.si","value","1084"],["map","key","www.wellneo.si","value","1272"],["map","key","www2.wellneo.si","value","1272"],["map","key","www.rovus.si","value","1287"],["map","key","www2.rovus.si","value","1287"]]
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",34],
      "vtp_map":["list",["map","key","http","value","http:\/\/d7.zedo.com\/img\/bh.gif?n=2585\u0026g=20\u0026a=52\u0026s=1\u0026l=1\u0026t=i"],["map","key","https","value","https:\/\/tt7.zedo.com\/img\/bh.gif?n=2585\u0026g=20\u0026a=52\u0026s=1\u0026l=1\u0026t=i"]]
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",34],
      "vtp_map":["list",["map","key","http","value","http:\/\/d7.zedo.com\/img\/bh.gif?n=2585\u0026g=20\u0026a=18\u0026s=1\u0026l=1\u0026t=i"],["map","key","https","value","https:\/\/tt7.zedo.com\/img\/bh.gif?n=2585\u0026g=20\u0026a=18\u0026s=1\u0026l=1\u0026t=i"]]
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"feeis"
    },{
      "function":"__c",
      "vtp_value":"766239161"
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false
    }],
  "tags":[{
      "function":"__html",
      "priority":99999,
      "once_per_load":true,
      "vtp_html":"\u003Cstyle\u003E\n#dd_attribute175 {\n  display: block !important;\n}\n\u003C\/style\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":162
    },{
      "function":"__html",
      "priority":10,
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(1\u003E$j(\".cms-index-index\").length\u00261\u003E$j(\".catalog-category-view\").length\u00261\u003E$j(\".catalog-product-view\").length\u00261\u003E$j(\".checkout-cart-index\").length\u00261\u003E$j(\".checkout-onepage-success\").length){var google_tag_params={dynx_pagetype:\"other\"};0\u003C$j(\".catalogsearch-result-index\").length\u0026\u0026(google_tag_params.dynx_pagetype=\"searchresults\");0\u003C$j(\".checkout-onepage-index\").length\u0026\u0026(google_tag_params.dynx_pagetype=\"cart\");dataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":131
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"879085990",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_conversionLabel":"WJc-CP6ssQMQqvOS1AM",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":99
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"879088195",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_conversionLabel":"yRwrCI3FiAIQm5n8_QM",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":100
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"879085801",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":101
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"879095593",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_conversionLabel":"Q5FMCLe2xAIQ4cSw0wM",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":102
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"879090870",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_conversionLabel":"dZAMCPatsQMQqvOS1AM",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":103
    },{
      "function":"__sp",
      "priority":5,
      "once_per_event":true,
      "vtp_dataLayerVariable":["macro",23],
      "vtp_conversionId":"766239161",
      "vtp_customParamsFormat":"DATA_LAYER",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",24],
      "tag_id":167
    },{
      "function":"__ua",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","page","value","\/cart3-UserForm-Logged"],["map","fieldName","allowLinker","value","false"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":62
    },{
      "function":"__ua",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","page","value",["macro",6]],["map","fieldName","allowLinker","value","false"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":63
    },{
      "function":"__ua",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","page","value","\/cart4-PaymentOptions"],["map","fieldName","allowLinker","value","false"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":64
    },{
      "function":"__cl",
      "tag_id":67
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Add to Cart",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":69
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Checkout",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":70
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Checkout Option",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":71
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":true,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Product Impressions",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Product Seen",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":73
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Product Click",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":74
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Product View",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":75
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":true,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Promotion Impression",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Promotion Seen",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":76
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",17]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Promotion Click",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":77
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useEcommerceDataLayer":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",16]]],
      "vtp_eventCategory":"Ecommerce",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"Remove from Cart",
      "vtp_enableEcommerce":true,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_ecommerceIsEnabled":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":78
    },{
      "function":"__jel",
      "tag_id":79
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":["macro",18],
      "vtp_eventCategory":["macro",19],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",20],
      "vtp_eventLabel":["macro",21],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":80
    },{
      "function":"__lcl",
      "vtp_waitForTagsTimeout":"2000",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "tag_id":82
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","1","group",["macro",10]]],
      "vtp_autoLinkDomains":"wellneo.si,dormeo.net,delimano.si,liveactive.si,topshop.si,walkmaxx.si",
      "vtp_decorateFormsAutoLink":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","true"],["map","fieldName","cookieDomain","value","auto"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",25]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":108
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":"wellneo.si,dormeo.net,delimano.si,liveactive.si,topshop.si,walkmaxx.si",
      "vtp_decorateFormsAutoLink":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","true"],["map","fieldName","cookieDomain","value","auto"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",25]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",26],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":114
    },{
      "function":"__ua",
      "unlimited":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_trackType":"TRACK_TRANSACTION",
      "vtp_enableLinkId":false,
      "vtp_trackingId":["macro",26],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsTransaction":true,
      "tag_id":115
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":["macro",18],
      "vtp_eventCategory":["macro",19],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",20],
      "vtp_eventLabel":["macro",21],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":128
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_dimension":["list",["map","index","5","dimension",["macro",27]]],
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":171
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",28]],["map","fieldName","title","value",["macro",29]],["map","fieldName","allowLinker","value","false"]],
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_trackingId":["macro",12],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":174
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2325283_230",
      "tag_id":175
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2325283_232",
      "tag_id":176
    },{
      "function":"__evl",
      "vtp_useOnScreenDuration":false,
      "vtp_useDomChangeListener":true,
      "vtp_elementSelector":".cart-crossell-products",
      "vtp_firingFrequency":"ONCE",
      "vtp_selectorType":"CSS",
      "vtp_onScreenRatio":"75",
      "vtp_uniqueTriggerId":"2325283_265",
      "tag_id":177
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(\"undefined\"!==typeof ",["escape",["macro",15],8,16],")$j(\".view-prod, .product-image, .widget-product-link, .productList-product-link, .header-product-link, .view-prod-sc, .softcubeHistory .widget-product-link\").on(\"click\",function(){var a=$j(this);a=a.parents(\".item\");var e=a.find(\".js-gtm-data\").data(\"engname\"),f=a.find(\".js-gtm-data\").data(\"cpr\"),d=0\u003Ca.find(\".special-price .price\").length?a.find(\".special-price .price\").text():a.find(\".regular-price .price, .old-price .price\").text();d=$j.trim(d.match(\/[0-9,.]+\/g)).replace(\".\",\n\"\").replace(\",\",\".\").replace(\" \",\"\");var b=\"\",g=",["escape",["macro",30],8,16],",c=0;0\u003Ca.parents(\".owl-wrapper\").length?(c=a.parent().index(),b=$j.trim(a.parents(\".widget-products-carousel\").find(\".recommended-heading\").text())+\" \"+$j.trim(a.parents(\".widget-products-carousel\").parent().parent().find(\".box-title\").text())):(c=a.parent().index(),b=a.parents(\"ul\").attr(\"id\"),\"undefined\"===typeof b\u0026\u0026(b=$j.trim($j(\".category-title h1, .page-title h1\").text())));c+=1;b=1\u003Eb.length?\"Recommended Products - not set\":\nb;b=0\u003Ca.parents(\".softcubeHistory\").length?\"Softcube Recommendation\":b;b=0\u003Ca.parents(\"#recently-viewed-items-wide-carousel\").length?\"recently-viewed\":b;0\u003C$j(\".catalogsearch-result-index\").length\u0026\u0026(b=\"Search \");dataLayer.push({event:\"productClick\",ecommerce:{click:{actionField:{list:b},products:[{name:e,id:f,price:d,brand:g,position:c}]}},eventCallback:function(){}})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":72
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version=\"2.0\",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,\"script\",\"\/\/connect.facebook.net\/en_US\/fbevents.js\");fbq(\"init\",\"",["escape",["macro",31],7],"\");fbq(\"track\",\"PageView\");var fbScriptInterval=setInterval(function(){fbTimer()},10);\nfunction fbTimer(){\"undefined\"!=typeof fbq\u0026\u0026clearInterval(fbScriptInterval)};\u003C\/script\u003E\n\u003Cnoscript\u003E\u003Cimg height=\"1\" width=\"1\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=",["escape",["macro",31],12],"\u0026amp;ev=PageView\u0026amp;noscript=1\"\u003E\u003C\/noscript\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":85
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(\"undefined\"!==typeof ",["escape",["macro",15],8,16],"){!function(a){a.fn.viewportChecker=function(e){var b={classToAdd:\"visible\",classToRemove:\"invisible\",classToAddForFullView:\"full-visible\",removeClassAfterAnimation:!1,offset:100,repeat:!1,invertBottomOffset:!0,callbackFunction:function(a,b){},scrollHorizontal:!1,scrollBox:window};a.extend(b,e);var h=this,m=a(b.scrollBox).height(),g=a(b.scrollBox).width();return this.checkElements=function(){var f,e;b.scrollHorizontal?(f=Math.max(a(\"html\").scrollLeft(),\na(\"body\").scrollLeft(),a(window).scrollLeft()),e=f+g):(f=Math.max(a(\"html\").scrollTop(),a(\"body\").scrollTop(),a(window).scrollTop()),e=f+m);h.each(function(){var c=a(this),d={},k={};if(c.data(\"vp-add-class\")\u0026\u0026(k.classToAdd=c.data(\"vp-add-class\")),c.data(\"vp-remove-class\")\u0026\u0026(k.classToRemove=c.data(\"vp-remove-class\")),c.data(\"vp-add-class-full-view\")\u0026\u0026(k.classToAddForFullView=c.data(\"vp-add-class-full-view\")),c.data(\"vp-keep-add-class\")\u0026\u0026(k.removeClassAfterAnimation=c.data(\"vp-remove-after-animation\")),\nc.data(\"vp-offset\")\u0026\u0026(k.offset=c.data(\"vp-offset\")),c.data(\"vp-repeat\")\u0026\u0026(k.repeat=c.data(\"vp-repeat\")),c.data(\"vp-scrollHorizontal\")\u0026\u0026(k.scrollHorizontal=c.data(\"vp-scrollHorizontal\")),c.data(\"vp-invertBottomOffset\")\u0026\u0026(k.scrollHorizontal=c.data(\"vp-invertBottomOffset\")),a.extend(d,b),a.extend(d,k),!c.data(\"vp-animated\")||d.repeat){0\u003CString(d.offset).indexOf(\"%\")\u0026\u0026(d.offset=parseInt(d.offset)\/100*m);k=d.scrollHorizontal?c.offset().left:c.offset().top;var h=d.scrollHorizontal?k+c.width():k+c.height(),\ng=Math.round(k)+d.offset,l=d.scrollHorizontal?g+c.width():g+c.height();(d.invertBottomOffset\u0026\u0026(l-=2*d.offset),g\u003Ce\u0026\u0026l\u003Ef)?(c.removeClass(d.classToRemove),c.addClass(d.classToAdd),g=d.listName,l=b.position+1,b.position=l,d.callbackFunction(c,\"add\",g,l),h\u003C=e\u0026\u0026k\u003E=f?c.addClass(d.classToAddForFullView):c.removeClass(d.classToAddForFullView),c.data(\"vp-animated\",!0),d.removeClassAfterAnimation\u0026\u0026c.one(\"webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend\",function(){c.removeClass(d.classToAdd)})):\nc.hasClass(d.classToAdd)\u0026\u0026d.repeat\u0026\u0026(c.removeClass(d.classToAdd+\" \"+d.classToAddForFullView),d.callbackFunction(c,\"remove\"),c.data(\"vp-animated\",!1))}})},(\"ontouchstart\"in window||\"onmsgesturechange\"in window)\u0026\u0026a(document).bind(\"touchmove MSPointerMove pointermove\",this.checkElements),a(b.scrollBox).bind(\"load scroll\",this.checkElements),a(window).resize(function(e){m=a(b.scrollBox).height();g=a(b.scrollBox).width();h.checkElements()}),this.checkElements(),this}}(jQuery);var updateImpressionsBlocks=\nfunction(){var a=[\".category-products\",\".widget-products-carousel\",\"#upsell-product-table\",\"#recently-viewed-items-wide-carousel\",\"#softcubeHistory\"];$j(a).each(function(a,b){0\u003C$j(b).length\u0026\u0026$j(b).each(function(a,e){var g=$j(this),f=\"\";g.hasClass(\"category-products\")?f=dataLayer[0].PageName:g.hasClass(\"widget-products-carousel\")?(f=$j.trim(g.find(\".recommended-heading\").text())+\" \"+g.parent().parent().find(\".box-title h2\").text(),f=0\u003Cf.length?f:g.find(\".recommended-heading h2\").text()):\"upsell-product-table\"==\ng.attr(\"id\")\u0026\u0026(f=\"upsell-product-table\");\"#recently-viewed-items-wide-carousel\"==b\u0026\u0026(f=\"recently-viewed\");\"#softcubeHistory\"==b\u0026\u0026(f=\"Softcube Recommendation\");0\u003C$j(\".catalogsearch-result-index\").length\u0026\u0026(f=\"Search \");g.find(\".js-gtm-data\").viewportChecker({classToAdd:\"visibleByEE\",offset:100,repeat:!1,listName:f,position:0,callbackFunction:function(a,b,d,e){var c=a.parents(\".item\");a=c.find(\".js-gtm-data\").data(\"engname\");b=c.find(\".js-gtm-data\").data(\"cpr\");c=0\u003Cc.find(\".special-price .price\").length?\nc.find(\".special-price .price\").text():c.find(\".regular-price .price, .old-price .price\").text();$j.trim(c.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");c={event:\"productImpression\",ecommerce:{impressions:[]}};c.ecommerce.impressions.push({id:b,name:a,category:d,list:d,position:e});dataLayer.push(c)},scrollHorizontal:!1})})})};updateImpressionsBlocks();if(0\u003C$j(\"#home-kv-slider li a, .home-page-sidebanner li a, .uni-banner a\").length){var promotions=[];0\u003C$j(\"#home-kv-slider li a\").length\u0026\u0026\n$j(\"#home-kv-slider li\").each(function(a,e){var b=$j(this);if(0\u003Cb.find(\"img\").length){var h=b.find(\"img\").attr(\"alt\");b=b.find(\"img\").attr(\"src\");promotions.push({id:\"HP Banners\",name:\"Main KV - \"+h,creative:b,position:a+1})}});0\u003C$j(\".home-page-sidebanner li a\").length\u0026\u0026$j(\".home-page-sidebanner li\").each(function(a,e){var b=$j(this);if(0\u003Cb.find(\"img\").length){var h=b.find(\"img\").attr(\"alt\");b=b.find(\"img\").attr(\"src\");promotions.push({id:\"HP Banners\",name:\"Right KV - \"+h,creative:b,position:a+1})}});\nif(0\u003C$j(\".uni-banner a\").length){var typebn=-1\u003Cwindow.location.pathname.indexOf(\"\/checkout\/cart\")?\"Cart\":\"Slim\";$j(\".uni-banner\").each(function(a,e){var b=$j(this);var h=$j(\".uni-banner .ub-txt-inner\").data(\"bannerdesc\");h=0\u003Cb.find(\".continue-with-your-purchase\").length?\"Cart - continue-with-your-purchase\":typebn+\" - \"+h;promotions.push({id:typebn+\" Banners\",name:h,creative:h,position:a+1})})}0\u003Cpromotions.length\u0026\u0026dataLayer.push({event:\"promotionView\",ecommerce:{promoView:{promotions:promotions}}})}$j(\"#home-kv-slider li a\").click(function(){var a=\n$j(this);0\u003Ca.find(\"img\").length||(a=a.parents(\"li\").find(\"a:first\"));var e=a.find(\"img\").attr(\"alt\");var b=a.find(\"img\").attr(\"src\");a=a.parents(\"li\").index()+1;dataLayer.push({event:\"promotionClick\",ecommerce:{promoClick:{promotions:[{id:\"HP Banners\",name:\"Main KV - \"+e,creative:b,position:a}]}},eventCallback:function(){}})});$j(\".home-page-sidebanner li a\").click(function(){var a=$j(this);0\u003Ca.find(\"img\").length||(a=a.parents(\"li\").find(\"a:first\"));var e=a.find(\"img\").attr(\"alt\");var b=a.find(\"img\").attr(\"src\");\na=a.parents(\"li\").index()+1;dataLayer.push({event:\"promotionClick\",ecommerce:{promoClick:{promotions:[{id:\"HP Banners\",name:\"Right KV - \"+e,creative:b,position:a}]}},eventCallback:function(){}})});$j(\".uni-banner a\").click(function(){var a=-1\u003Cwindow.location.pathname.indexOf(\"\/checkout\/cart\")?\"Cart\":\"Slim\",e=$j(this);var b=$j(\".uni-banner .ub-txt-inner\").data(\"bannerdesc\");b=e.hasClass(\"continue-with-your-purchase\")?\"Cart - continue-with-your-purchase\":a+\" - \"+b;var h=1;e.hasClass(\"continue-with-your-purchase\")\u0026\u0026\ndataLayer.push({event:\"promotionClick\",ecommerce:{promoClick:{promotions:[{id:a+\" Banner\",name:b,creative:b,position:h}]}},eventCallback:function(){}})});$j(\".uni-banner.link\").click(function(){var a=-1\u003Cwindow.location.pathname.indexOf(\"\/checkout\/cart\")?\"Cart\":\"Slim\",e=$j(this);var b=$j(\".uni-banner .ub-txt-inner\").data(\"bannerdesc\");b=0\u003Ce.find(\".continue-with-your-purchase\").length?\"Cart - continue-with-your-purchase\":a+\" - \"+b;e=1;dataLayer.push({event:\"promotionClick\",ecommerce:{promoClick:{promotions:[{id:a+\n\" Banner\",name:b,creative:b,position:e}]}},eventCallback:function(){}})})};\u003C\/script\u003E  "],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":91
    },{
      "function":"__html",
      "unlimited":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar eurUaAllTtrans=1,prUaAllTtrans=[];\nif(\"undefined\"!==typeof dataLayer[0].ecommerce){jQuery(\".cart-details .js-gtm-data\").each(function(a,d){var b={},c=jQuery(this);\"undefined\"!==typeof dataLayer[0].transactionProducts[a]\u0026\u0026(b.name=c.data(\"engname\"),b.id=c.data(\"cpr\"),\"undefined\"!==typeof dataLayer[0].transactionProducts[a].sku\u0026\u0026(b.sku=dataLayer[0].transactionProducts[a].sku),b.price=(dataLayer[0].transactionProducts[a].price*eurUaAllTtrans).toFixed(2),b.category=dataLayer[0].transactionProducts[a].category,\"undefined\"!==typeof dataLayer[0].transactionProducts[a].brand\u0026\u0026\n(b.brand=dataLayer[0].transactionProducts[a].brand),\"undefined\"!==typeof dataLayer[0].transactionProducts[a].variant\u0026\u0026(b.variant=dataLayer[0].transactionProducts[a].variant),b.quantity=dataLayer[0].transactionProducts[a].quantity,b.currency=\"EUR\",prUaAllTtrans.push(b))});var dlUaAllTtrans={event:\"trackTransUAALL\",ecommerce:{purchase:{actionField:{id:dataLayer[0].ecommerce.purchase.actionField.id,affiliation:dataLayer[0].ecommerce.purchase.actionField.affiliation+\"\",revenue:(dataLayer[0].ecommerce.purchase.actionField.revenue*\neurUaAllTtrans).toFixed(2),tax:(dataLayer[0].ecommerce.purchase.actionField.tax*eurUaAllTtrans).toFixed(2),shipping:(dataLayer[0].ecommerce.purchase.actionField.shipping*eurUaAllTtrans).toFixed(2),coupon:dataLayer[0].transactionPromoCode+\"\",currency:\"EUR\"},products:prUaAllTtrans}}};dataLayer.push(dlUaAllTtrans)}\n\"undefined\"!==typeof dataLayer[0].transactionId\u0026\u0026(jQuery(\".cart-details .js-gtm-data\").each(function(a,d){var b={},c=jQuery(this);\"undefined\"!==typeof dataLayer[0].transactionProducts[a]\u0026\u0026(b.name=c.data(\"engname\"),b.id=c.data(\"cpr\"),\"undefined\"!==typeof dataLayer[0].transactionProducts[a].sku\u0026\u0026(b.sku=dataLayer[0].transactionProducts[a].sku),b.price=(dataLayer[0].transactionProducts[a].price*eurUaAllTtrans).toFixed(2),b.category=dataLayer[0].transactionProducts[a].category,\"undefined\"!==typeof dataLayer[0].transactionProducts[a].brand\u0026\u0026\n(b.brand=dataLayer[0].transactionProducts[a].brand),\"undefined\"!==typeof dataLayer[0].transactionProducts[a].variant\u0026\u0026(b.variant=dataLayer[0].transactionProducts[a].variant),b.quantity=dataLayer[0].transactionProducts[a].quantity,b.currency=\"EUR\",prUaAllTtrans.push(b))}),dlUaAllTtrans={event:\"trackTransUAALL\",transactionId:dataLayer[0].transactionId,transactionAffiliation:dataLayer[0].transactionAffiliation+\"\",transactionTotal:(dataLayer[0].transactionTotal*eurUaAllTtrans).toFixed(2),transactionTax:(dataLayer[0].transactionTax*\neurUaAllTtrans).toFixed(2),transactionShipping:(dataLayer[0].transactionShipping*eurUaAllTtrans).toFixed(2),transactionPromoCode:dataLayer[0].transactionPromoCode+\"\",transactionCurrency:\"EUR\",transactionProducts:prUaAllTtrans},dataLayer.push(dlUaAllTtrans));\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":116
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E!function(b,a,c,d){b=a.createElement(c);b.async=1;b.src=\"https:\/\/script.softcube.com\/\"+d+\"\/sc.js\";a=a.scripts[0];a.parentNode.insertBefore(b,a)}(window,document,\"script\",\"",["escape",["macro",32],7],"\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":124
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript\u003Eif(0\u003C$j(\".cms-imemory-s\").length\u0026\u00260\u003C$j(\".js-gtm-data\").length){var ecommerceDetailView=function(){var a=$j(\".product-shop .js-gtm-data\").data(\"engname\"),b=$j(\".product-shop .js-gtm-data\").data(\"cpr\"),c=0\u003C$j(\".product-shop .special-price .price\").length?$j(\".product-shop .special-price .price\").text():$j(\".product-shop .regular-price .price,  .product-shop .old-price .price\").text();c=$j.trim(c.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var d=",["escape",["macro",30],8,16],",e=\"cms-imemory-s\";\ndataLayer.push({event:\"productView\",ecommerce:{detail:{actionFields:{list:e},products:[{name:a,id:b,price:c,brand:d}]}}})};\"undefined\"!==typeof ",["escape",["macro",15],8,16],"\u0026\u0026(ecommerceDetailView(),$j(\".add-to-cart-buttons ,.button-slide-buy \").on(\"click\",function(){var a=$j(this);a.parents(\".item\");var b=$j(\".product-shop .js-gtm-data\").data(\"engname\"),c=$j(\".product-shop .js-gtm-data\").data(\"cpr\"),d=0\u003C$j(\".product-shop .special-price .price\").length?$j(\".product-shop .special-price .price\").text():$j(\".product-shop .regular-price .price, .product-shop .old-price .price\").text();\nd=$j.trim(d.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var e=$j(\"select.qty\").val(),g=",["escape",["macro",30],8,16],";a=",["escape",["macro",33],8,16],";var f=[];f.push({name:b,id:c,price:d,brand:g,quantity:e});b=\"cms-imemory-s\";dataLayer.push({event:\"addToCart\",ecommerce:{currencyCode:a,add:{actionFields:{list:b},products:f}}})}))};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":129
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".catalog-category-view\").length){var gaadwcat=$j(\".catalog-category-view .category-title h1\").text(),google_tag_params={dynx_pagetype:\"category\",dynx_pcat:[gaadwcat]};dataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":130
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".checkout-cart-index\").length){var listskus=[];$j(\".cart-item-wrapper .cart-item-table\").each(function(b,c){var a=$j(this).find(\".js-gtm-data\").data(\"cpr\");listskus.push($j.trim(a))});var google_tag_params={dynx_pagetype:\"cart\",dynx_itemid:listskus,dynx_itemid2:\"\",dynx_totalvalue:$j.trim($j(\"#checkout-totals-marker-grandtotal\").first().find(\".price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\")};dataLayer.push({event:\"remarketingTriggered\",google_tag_params:window.google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":132
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".checkout-cart-index\").length\u0026\u0026\"undefined\"!==typeof ",["escape",["macro",15],8,16],"){$j(\"#add1, #minus1, .btn-remove, .remove-extwarranty\").on(\"click\",function(){var f=\"",["escape",["macro",33],7],"\",e=$j(this),a=e.parents(\".cart-item-wrapper .cart-item-table\"),c=a.find(\".js-gtm-data\").data(\"engname\"),d=a.find(\".js-gtm-data\").data(\"cpr\"),b=a.find(\".cart-item-total .cart-price .price\").text();b=$j.trim(b.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");a=a.find(\".input-text.qty\").val();\nvar g=",["escape",["macro",30],8,16],";e.hasClass(\"plus\")?dataLayer.push({event:\"addToCart\",ecommerce:{currencyCode:f,add:{products:[{name:c,id:d,price:b,brand:g,quantity:a}]}}}):dataLayer.push({event:\"removeFromCart\",ecommerce:{remove:{products:[{name:c,id:d,price:b,brand:g,quantity:a}]}}})});var localcurrency=",["escape",["macro",33],8,16],",brand=",["escape",["macro",30],8,16],",productsInBasket=[];$j(\".cart-item-wrapper .cart-item-table\").each(function(f,e){var a=$j(this),c=a.find(\".js-gtm-data\").data(\"engname\"),d=a.find(\".js-gtm-data\").data(\"cpr\"),\nb=a.find(\".cart-item-total .cart-price .price\").text();b=$j.trim(b.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");a=a.find(\".input-text.qty\").val();productsInBasket.push({name:c,id:d,price:b,brand:brand,quantity:a})});dataLayer.push({event:\"checkout\",ecommerce:{checkout:{actionField:{step:1},products:productsInBasket}},eventCallback:function(){}})};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":133
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".checkout-onepage-index\").length\u0026\u0026\"undefined\"!==typeof ",["escape",["macro",15],8,16],"){var utuaec=0\u003C$j(\"#billing-address-select\").length?\"logged-yes\":\"logged-no\";dataLayer.push({event:\"checkout\",ecommerce:{checkout:{actionField:{step:2,option:\"CO VIEW: \"+utuaec}}}});dataLayer.push({event:\"checkoutOption\",ecommerce:{checkout_option:{actionField:{step:2,option:\"CO VIEW: \"+utuaec}}}});Checkout.prototype.gotoSection=Checkout.prototype.gotoSection.wrap(function(a,b,c){a(b,c);\"undefined\"!=typeof response\u0026\u0026\n\"undefined\"!=typeof response.goto_section\u0026\u0026gotoSectionSection(response.goto_section)});$j(\".cart3_payment_options\").on(\"click\",function(){var a=$j(\"input[name\\x3d'payment[method]']:checked\",\"#co-payment-form\").val();dataLayer.push({event:\"checkout\",ecommerce:{checkout:{actionField:{step:4,option:\"CO PAYMENT: \"+a}}}});dataLayer.push({event:\"checkoutOption\",ecommerce:{checkout_option:{actionField:{step:4,option:\"CO PAYMENT: \"+a}}}})})}\nfunction gotoSectionSection(a){if(\"payment\"==a\u0026\u0026$j(\"#opc-payment\").hasClass(\"active\")){a=$j(\"input[name\\x3dshipping-type]:checked\",\"#co-billing-form\").val();\"undefined\"===typeof a\u0026\u0026(a=\"not available\");var b=$j(\"input[name\\x3dcheckout_method_splitcart]:checked\",\"#checkout-step-billing\").val();\"undefined\"==typeof b\u0026\u0026(b=0\u003C$j(\"#billing-address-select\").length?\"authenticated\":b);dataLayer.push({event:\"checkout\",ecommerce:{checkout:{actionField:{step:3,option:\"CO SELECT: \"+b+\" | \"+a}}}});dataLayer.push({event:\"checkoutOption\",\necommerce:{checkout_option:{actionField:{step:3,option:\"CO SELECT: \"+b+\" | \"+a}}}})}};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":134
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".cms-index-index\").length){var google_tag_params={dynx_pagetype:\"home\"};dataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":135
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".catalog-product-view, .redboxdigital-quickview-index-index\").length\u0026\u00260\u003C$j(\".js-gtm-data\").length){var prceval=0;prceval=0\u003C$j(\".product-shop .regular-price\").length?$j.trim($j(\".product-shop .regular-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\"):$j.trim($j(\".product-shop .special-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var google_tag_params={dynx_pagetype:\"product\",dynx_itemid:[$j.trim($j(\".product-essential .js-gtm-data\").data(\"cpr\"))],\ndynx_totalvalue:prceval};dataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":136
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".catalog-product-view, .redboxdigital-quickview-index-index\").length\u0026\u00260\u003C$j(\".js-gtm-data\").length){var ecommerceDetailView=function(){var d=$j(\".product-essential .js-gtm-data\").data(\"engname\"),c=$j(\".product-essential .js-gtm-data\").data(\"cpr\"),a=0\u003C$j(\".product-essential .special-price .price\").length?$j(\".product-essential .special-price .price\").text():$j(\".product-essential .regular-price .price,  .product-essential .old-price .price\").text();a=$j.trim(a.match(\/[0-9,.]+\/g)).replace(\".\",\n\"\").replace(\",\",\".\").replace(\" \",\"\");var b=",["escape",["macro",30],8,16],",e=$j(\"body\").hasClass(\"catalog-product-view\")?\"catalog-product-view\":\"quickview\";dataLayer.push({event:\"productView\",ecommerce:{detail:{actionFields:{list:e},products:[{name:d,id:c,price:a,brand:b}]}}})};\"undefined\"!==typeof ",["escape",["macro",15],8,16],"\u0026\u0026(ecommerceDetailView(),$j(\".add-to-cart-buttons ,.button-slide-buy \").on(\"click\",function(){var d=$j(this);d.parents(\".item\");var c=$j(\".product-essential .js-gtm-data\").data(\"engname\"),a=\n$j(\".product-essential .js-gtm-data\").data(\"cpr\"),b=0\u003C$j(\".product-essential .special-price .price\").length?$j(\".product-essential .special-price .price\").text():$j(\".product-essential .regular-price .price, .product-essential .old-price .price\").text();b=$j.trim(b.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var e=$j(\".input-text.qty\").val(),h=",["escape",["macro",30],8,16],";d=",["escape",["macro",33],8,16],";var f=[];f.push({name:c,id:a,price:b,brand:h,quantity:e});if(jQuery(\"input[type\\x3d'radio'].extended-warranty\").is(\":checked\")){a=\n\"EXTENDED WARRANTY \/ \"+c;b=jQuery(\"input[type\\x3d'hidden'].selected_warranty_sku\").val();var g=jQuery(\".warrantySelect.active .price\").text();g=jQuery.trim(g.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");void 0!==jQuery(\"input[type\\x3d'radio'].extended-warranty\").attr(\"data-engname\")\u0026\u0026(a=jQuery(\"input[type\\x3d'radio'].extended-warranty:checked\").data(\"engname\")+\" \/ \"+c);void 0!==jQuery(\"input[type\\x3d'radio'].extended-warranty\").attr(\"data-cpr\")\u0026\u0026(b=jQuery(\"input[type\\x3d'radio'].extended-warranty:checked\").data(\"cpr\"));\nf.push({name:a,id:b,price:g,brand:h,quantity:e})}c=$j(\"body\").hasClass(\"catalog-product-view\")?\"catalog-product-view\":\"quickview\";dataLayer.push({event:\"addToCart\",ecommerce:{currencyCode:d,add:{actionFields:{list:c},products:f}}})}))};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":137
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".checkout-onepage-success\").length){var itemskus=[],itemskusname=[],itemskusprice=[],itms=[];$j(\".cart-details .js-gtm-data\").each(function(a,c){var b=$j(this).data(\"cpr\");itemskus.push(b);itms.push({productid:b,step:3})});dataLayer[0].transactionProducts.each(function(a,c){itemskusname.push(a.name);itemskusprice.push(a.price)});var google_tag_params={dynx_pagetype:\"purchase\",dynx_pcat:[],dynx_itemid:itemskus,dynx_pname:itemskusname,dynx_pvalue:itemskusprice,dynx_totalvalue:dataLayer[0].transactionTotal};\ndataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":138
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".checkout-onepage-success\").length\u0026\u0026\"undefined\"!==typeof ",["escape",["macro",15],8,16],"){var ecommercePurchase=function(){var d=[];$j(\".cart-details .js-gtm-data\").each(function(b,c){var a={},e=$j(this);\"undefined\"!==typeof dataLayer[0].transactionProducts[b]\u0026\u0026(a.name=e.data(\"engname\"),a.id=e.data(\"cpr\"),a.price=dataLayer[0].transactionProducts[b].price,a.category=dataLayer[0].transactionProducts[b].category,\"undefined\"!==typeof dataLayer[0].transactionProducts[b].brand\u0026\u0026(a.brand=dataLayer[0].transactionProducts[b].brand),\n\"undefined\"!==typeof dataLayer[0].transactionProducts[b].variant\u0026\u0026(a.variant=dataLayer[0].transactionProducts[b].variant),a.quantity=dataLayer[0].transactionProducts[b].quantity,a.currency=\"",["escape",["macro",33],7],"\",a.brand=",["escape",["macro",30],8,16],",d.push(a))});var c={event:\"checkout\",ecommerce:{purchase:{actionField:{id:dataLayer[0].transactionId,affiliation:dataLayer[0].transactionAffiliation+\"\",revenue:dataLayer[0].transactionTotal.toFixed(2),tax:dataLayer[0].transactionTax.toFixed(2),shipping:dataLayer[0].transactionShipping.toFixed(2),\ncurrency:\"",["escape",["macro",33],7],"\"},products:d}}};\"undefined\"!==typeof dataLayer[0].transactionPromoCode\u0026\u0026(c.ecommerce.purchase.actionField.coupon=dataLayer[0].transactionPromoCode+\"\");dataLayer.push(c)};ecommercePurchase()};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":139
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript\u003E\"function\"==typeof tawkChatInit\u0026\u0026tawkChatInit();\"function\"==typeof initClickToCall\u0026\u0026initClickToCall();\"function\"==typeof initPimcoreBanners\u0026\u0026initPimcoreBanners();\"function\"==typeof intiGoogleScripts\u0026\u0026(intiGoogleScripts(),jQuery(\".store-locator-marker\").removeClass(\"no-display\"));(function(){var a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"",["escape",["macro",35],7],"\";document.body.appendChild(a)})();var silverpopVar=setInterval(function(){silverpopTimer()},10);\nfunction silverpopTimer(){if(\"undefined\"!=typeof ewt){clearInterval(silverpopVar);var a=getCookie(\"com.silverpop.iMA.session\")?getCookie(\"com.silverpop.iMA.session\"):\"\";\"\"==a\u0026\u0026ewt.init();if(0\u003C$j(\".checkout-onepage-success\").length){var b=[];$j(\".cart-details .js-gtm-data\").each(function(a,c){var d=$j(this).data(\"cpr\");b.push(d)});a=b.join(\",\");var c=dataLayer[0].transactionTotal;cotData={action:\"Purchase\"};cotData.detail=\"Pids:\"+a;cotData.amount=String(c);ewt.cot(cotData)}}};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":141
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript\u003Eif(0\u003C$j(\".cms-imemory-s\").length\u0026\u00260\u003C$j(\".js-gtm-data\").length){var prceval=0;prceval=0\u003C$j(\".product-shop .regular-price\").length?$j.trim($j(\".product-shop .regular-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\"):$j.trim($j(\".product-shop .old-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var google_tag_params={dynx_pagetype:\"product\",dynx_itemid:[$j.trim($j(\".product-shop .js-gtm-data\").data(\"cpr\"))],dynx_totalvalue:prceval};\ndataLayer.push({event:\"remarketingTriggered\",google_tag_params:google_tag_params})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":142
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".catalog-product-view\").length){var fbname=$j(\".product-name h1\").text(),fbcprid=$j(\".product-essential .js-gtm-data\").data(\"cpr\").toString(),fbprice=0\u003C$j(\".product-buybox-table .special-price .price\").length?$j(\".product-buybox-table .special-price .price\").text():$j(\".product-buybox-table .regular-price .price,  .product-buybox-table .old-price .price\").text();fbprice=$j.trim(fbprice.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");var fbcategory=0\u003C$j(\".breadcrumbs ul li\").eq(1).find(\"a\").length?\n$j(\".breadcrumbs ul li\").eq(1).find(\"a\").text():",["escape",["macro",30],8,16],";fbq(\"track\",\"AddToCart\",{content_name:fbname,content_category:fbcategory,content_ids:[fbcprid],content_type:\"product\",value:fbprice,currency:\"",["escape",["macro",33],7],"\"})};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":143
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar fbproducts=[],fbtotalprice=0;$j(\".cart-details .js-gtm-data\").each(function(a,b){fbproducts.push($j(this).data(\"cpr\").toString())});fbq(\"track\",\"Purchase\",{content_ids:fbproducts,content_type:\"product\",value:dataLayer[0].transactionTotal.toFixed(2),currency:\"",["escape",["macro",33],7],"\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":144
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efbq(\"trackCustom\",\"EmptyCart\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":145
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar fbcategoryproducts=[],categoryid=$j(\".category-products\").data(\"catid\");$j(\".category-products .product-info\").each(function(b,c){var a=$j(this);a=a.find(\".js-gtm-data\");0\u003Ca.length\u0026\u0026fbcategoryproducts.push(a.data(\"cpr\").toString())});0\u003Cfbcategoryproducts.length\u0026\u0026fbq(\"trackCustom\",\"Category\",{content_type:\"product\",category_id:categoryid,content_ids:fbcategoryproducts});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":146
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar gtmGetUrlParameter=function(d){var b=decodeURIComponent(window.location.search.substring(1));b=b.split(\"\\x26\");var a;for(a=0;a\u003Cb.length;a++){var c=b[a].split(\"\\x3d\");if(c[0]===d)return void 0===c[1]?!0:c[1]}},fbsearchproducts=[],searchTerm=gtmGetUrlParameter(\"q\");$j(\".category-products .product-info\").each(function(d,b){var a=$j(this);a=a.find(\".js-gtm-data\");0\u003Ca.length\u0026\u0026fbsearchproducts.push(a.data(\"cpr\").toString())});\nfbq(\"trackCustom\",\"Search\",{content_type:\"product\",content_ids:fbsearchproducts,search_term:searchTerm});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":147
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,1]],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar fbproducts=[],fbtotalprice=0,totalItems=0,quantity=0;$j(\".cart-item-wrapper .cart-item-table\").each(function(b,c){var a=$j(this);a=a.find(\".js-gtm-data\");0\u003Ca.length\u0026\u0026fbproducts.push(a.data(\"cpr\").toString())});$j(\".cart-item-wrapper .cart-item-table .quantity input.qty\").each(function(b,c){quantity=$j(this).val();totalItems+=Number(quantity)});fbtotalprice=$j(\".cart-totals-wrapper #checkout-totals-marker-grandtotal .price\").text();\nfbtotalprice=$j.trim(fbtotalprice.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");0\u003Cfbproducts.length\u0026\u0026fbq(\"track\",\"InitiateCheckout\",{content_name:\"Shopping Cart Checkout\",content_type:\"product\",content_ids:fbproducts,value:fbtotalprice,num_items:totalItems,currency:\"",["escape",["macro",33],7],"\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":148
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EjQuery(document).ready(function(){jQuery(\".btn-proceed-checkout\").click(function(){dataLayer.push({event:\"ClickBtnProceedCheckout\"})});jQuery(\".checkout-cart-index .uni-banner-button\").click(function(){dataLayer.push({event:\"ClickBtnProceedCheckout\"})});jQuery(\".cart3_payment_options\").click(function(){dataLayer.push({event:\"AddPaymentInfo\"})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":149
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,1]],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar fbtotalprice=0;fbtotalprice=$j(\"#shopping-cart-totals-table #checkout-totals-marker-grandtotal .price\").text();fbtotalprice=$j.trim(fbtotalprice.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");fbq(\"track\",\"AddPaymentInfo\",{content_name:\"Shopping Cart Payment Info\",content_type:\"product\",value:fbtotalprice,currency:\"",["escape",["macro",33],7],"\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":150
    },{
      "function":"__html",
      "setup_tags":["list",["tag",34,0]],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar fbname=$j(\".product-name h1\").text(),fbcprid=$j(\".product-essential .js-gtm-data\").data(\"cpr\").toString(),fbprice=0\u003C$j(\".product-buybox-table .special-price .price\").length?$j(\".product-buybox-table .special-price .price\").text():$j(\".product-buybox-table .regular-price .price,  .product-buybox-table .old-price .price\").text();fbprice=$j.trim(fbprice.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");\nvar fbcategory=0\u003C$j(\".breadcrumbs ul li\").eq(1).find(\"a\").length?$j(\".breadcrumbs ul li\").eq(1).find(\"a\").text():",["escape",["macro",30],8,16],";fbq(\"track\",\"ViewContent\",{content_name:fbname,content_category:fbcategory,content_ids:[fbcprid],content_type:\"product\",value:fbprice,currency:\"",["escape",["macro",33],7],"\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":151
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003CjQuery(\".webformTrack\").length){var category=jQuery(\".webformTrack\").data(\"gacategory\"),action=jQuery(\".webformTrack\").data(\"gaaction\"),label=jQuery(\".webformTrack\").data(\"galabel\");\"undefined\"!=typeof dataLayer\u0026\u00260\u003Ccategory.length\u0026\u00260\u003Caction.length\u0026\u00260\u003Clabel.length\u0026\u0026dataLayer.push({event:\"GAEvent\",eventCategory:category,eventAction:action,eventLabel:label})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":156
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"GAEvent\",eventCategory:\"Black Friday\",eventAction:\"Subscription\",eventLabel:\"Page\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":157
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"GAEvent\",eventCategory:\"Black Friday\",eventAction:\"Link To Subscription\",eventLabel:\"Floater\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":158
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"GAEvent\",eventCategory:\"Black Friday\",eventAction:\"Main-Cat\",eventLabel:\"Main-Cat\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":159
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript\u003E(function(b,a,f,c){b[c]=b[c]||function(){\"undefined\"==typeof ipromNS?(b[c].q=b[c].q||[]).push(arguments):ipromNS.execute(arguments)};var e=\"script\",d=a.createElement(e);a=a.getElementsByTagName(e)[0];d.async=!0;d.src=f+\"?\"+1*new Date;a.parentNode.insertBefore(d,a)})(window,document,\"\/\/www.ipromcloud.com\/ipromNS.js\",\"_ipromNS\");_ipromNS(\"config\",{sitePath:[(new String(",["escape",["macro",30],8,16],")).toLowerCase()],cookieConsent:!0,remarketing:!0});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":163
    },{
      "function":"__html",
      "setup_tags":["list",["tag",63,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003C$j(\".catalog-product-view\").length\u0026\u00260\u003C$j(\".js-gtm-data\").length){var cpr=$j.trim($j(\".product-essential .js-gtm-data\").data(\"cpr\")),prceval=0;prceval=0\u003C$j(\".product-shop .regular-price\").length?$j.trim($j(\".product-shop .regular-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\"):$j.trim($j(\".product-shop .old-price .price\").text().match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");\"undefined\"!==typeof _ipromNS\u0026\u0026_ipromNS(\"track\",\"ViewContent\",\n{content_ids:[cpr],content_type:\"product\",value:prceval,currency:\"EUR\"})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":164
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){if($j(\".product-essential\")){var b=$j.trim($j(\".product-essential .js-gtm-data\").data(\"cpr\")),a=0\u003C$j(\".product-essential .special-price .price\").length?$j(\".product-essential .special-price .price\").text():$j(\".product-essential .regular-price .price, .product-essential .old-price .price\").text();a=$j.trim(a.match(\/[0-9,.]+\/g)).replace(\".\",\"\").replace(\",\",\".\").replace(\" \",\"\");\"undefined\"!==typeof _ipromNS\u0026\u0026_ipromNS(\"track\",\"AddToCart\",{content_ids:[b],content_type:\"product\",value:a,currency:\"EUR\"})}})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":165
    },{
      "function":"__html",
      "setup_tags":["list",["tag",63,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E0\u003C$j(\".checkout-onepage-success\").length\u0026\u0026\"undefined\"!==typeof _ipromNS\u0026\u0026dataLayer[0].transactionProducts.each(function(a,f){for(var c=a.sku,d=a.price,e=parseInt(a.quantity),b=0;b\u003Ce;b++)_ipromNS(\"track\",\"Purchase\",{content_ids:[c],content_type:\"product\",value:d,currency:\"EUR\"})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":166
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar mapping=[\"show\",\"hide\"],crosssellcookie=",["escape",["macro",38],8,16],";switch(mapping[crosssellcookie]){case \"show\":break;case \"hide\":break;default:randomCookieSetter()}function randomCookieSetter(){var a=",["escape",["macro",39],8,16],";1073741823\u003Ca?setEngine(0):setEngine(1)}function setEngine(a){ga(\"send\",{hitType:\"event\",eventCategory:\"CrossSellsSplit\",eventAction:\"setGroup:\"+mapping[a],eventLabel:\"CrossSellsSplitSetGroup\"});dataLayer.push({crossellSplitEvent:\"setGroup\"});createCookie(a)}\nfunction createCookie(a){var b=new Date;b.setTime(b.getTime()+63072E6);b=\"expires\\x3d\"+b.toGMTString();document.cookie=\"crsssptst\\x3d\"+a+\"; \"+b+\"; path\\x3d\/\"};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":168
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar mapping=[\"show\",\"hide\"],selectedEngine=",["escape",["macro",38],8,16],";function trackDisplay(a){ga(\"send\",{hitType:\"event\",eventCategory:\"CrossSellsSplit\",eventAction:\"crossselsDisplay:\"+mapping[a],eventLabel:\"CrossSellsSplitDisplay\"});dataLayer.push({CrossSellsSplitEvent:\"crossselsDisplay\"})};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":169
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Eif(0\u003CjQuery(\".checkout-cart-index\").length\u0026\u00260\u003CjQuery(\".crosssell\").length){var products=[],gaProducts=[],blockid=jQuery(\".crosssell\").data(\"blockid\")+\"_1\",iterate=jQuery(\".crosssell .js-gtm-data\").each(function(c,d){var a=jQuery(this).data(\"cpr\"),b={product_id:a.toString(),container_type:blockid};gaProducts.push(a.toString());products.push(b)});jQuery.when(iterate).then(sendImpresionsToSoftcube(products,gaProducts))}\nfunction sendImpresionsToSoftcube(c,d){var a=d.toString();dataLayer.push({event:\"GAEvent\",eventCategory:\"CrossSells\",eventAction:\"Seen\",eventLabel:a});var b=setInterval(function(){\"object\"===typeof _sc\u0026\u0026(clearInterval(b),_sc.sendEvent(\"ProductImpression\",{ProductImpression:c}))},100);setTimeout(function(){clearInterval(b)},3E3)};\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":170
    }],
  "predicates":[{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"CartUserFormLoggedIn"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"gtm.load"
    },{
      "function":"_cn",
      "arg0":["macro",13],
      "arg1":"gtm.click"
    },{
      "function":"_eq",
      "arg0":["macro",1],
      "arg1":"true"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"CartPayment"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"CartPayment"
    },{
      "function":"_re",
      "arg0":["macro",14],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"gtm.js"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"addToCart"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"1"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"gtm.dom"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"checkout"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"checkoutOption"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"productImpression"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"productClick"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"productView"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"promotionView"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"promotionClick"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"removeFromCart"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"GAEvent"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"remarketingTriggered"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"delimano.si"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"remarketingTriggered"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"dormeo.net"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"wellneo.si"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"topshop.si"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"walkmaxx.si"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"trackTransUAALL"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"ScrollDistance"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"ScrollDistance"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"rovus.si"
    },{
      "function":"_eq",
      "arg0":["macro",27],
      "arg1":"Show"
    },{
      "function":"_eq",
      "arg0":["macro",27],
      "arg1":"Hide"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"VirtualPageview"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"implicitConsentGiven"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"implicitConsentGiven"
    },{
      "function":"_cn",
      "arg0":["macro",17],
      "arg1":"\/checkout\/onepage\/success\/"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"superstore.si"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"true"
    },{
      "function":"_eq",
      "arg0":["macro",4],
      "arg1":"true"
    },{
      "function":"_cn",
      "arg0":["macro",17],
      "arg1":"\/checkout\/cart"
    },{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"false"
    },{
      "function":"_re",
      "arg0":["macro",17],
      "arg1":"\/checkout\/onepage\/$"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"HomePage"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"addToCart"
    },{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"true"
    },{
      "function":"_cn",
      "arg0":["macro",17],
      "arg1":"\/catalogsearch\/result"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"ClickBtnProceedCheckout"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"ClickBtnProceedCheckout"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"AddPaymentInfo"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"AddPaymentInfo"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"webformSubmited"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"webformSubmited"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"true"
    },{
      "function":"_cn",
      "arg0":["macro",36],
      "arg1":"ga-event-link"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"gtm.linkClick"
    },{
      "function":"_re",
      "arg0":["macro",37],
      "arg1":"(^$|((^|,)2325283_230($|,)))"
    },{
      "function":"_cn",
      "arg0":["macro",36],
      "arg1":"fat-cat-click"
    },{
      "function":"_re",
      "arg0":["macro",37],
      "arg1":"(^$|((^|,)2325283_232($|,)))"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"explicitConsentGiven"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"explicitConsentGiven"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"gtm.elementVisibility"
    },{
      "function":"_re",
      "arg0":["macro",37],
      "arg1":"(^$|((^|,)2325283_265($|,)))"
    }],
  "rules":[
    [["if",0,1],["add",8]],
    [["if",2,3,4],["add",9]],
    [["if",5,6],["add",10]],
    [["if",7,8],["add",11,21,23,24,67]],
    [["if",9],["add",12]],
    [["if",12],["add",13]],
    [["if",13],["add",14]],
    [["if",14],["add",15]],
    [["if",15],["add",16]],
    [["if",16],["add",17]],
    [["if",17],["add",18]],
    [["if",18],["add",19]],
    [["if",19],["add",20]],
    [["if",20],["add",22]],
    [["if",21,22,23],["add",2]],
    [["if",21,23,24],["add",3]],
    [["if",21,23,25],["add",4]],
    [["if",21,23,26],["add",5]],
    [["if",21,23,27],["add",6]],
    [["if",8],["add",25,33,35,30,31,32]],
    [["if",28],["add",26]],
    [["if",29,30],["add",27]],
    [["if",21,23,31],["add",7]],
    [["if",8,32],["add",28]],
    [["if",8,33],["add",28]],
    [["if",34],["add",29]],
    [["if",35,36],["add",34,37,1,48,56],["block",1]],
    [["if",11,37],["add",36,46,47]],
    [["if",35,36,39],["add",38,44,49,58,64]],
    [["if",35,36,40],["add",39,53]],
    [["if",35,36,41,42],["add",40]],
    [["if",11,41,42],["add",41,68]],
    [["if",11,43],["add",42]],
    [["if",35,36,44],["add",43]],
    [["if",11,39],["add",45,0]],
    [["if",9,45],["add",50,65]],
    [["if",35,36,37],["add",51,66]],
    [["if",35,36,41,46],["add",52]],
    [["if",35,36,47],["add",54]],
    [["if",48,49],["add",55]],
    [["if",50,51],["add",57]],
    [["if",52,53],["add",59]],
    [["if",53,54],["add",60]],
    [["if",55,56,57],["add",61]],
    [["if",56,58,59],["add",62]],
    [["if",60,61],["add",63]],
    [["if",62,63],["add",69]],
    [["if",11],["unless",10],["block",12,13,14,15,16,17,18,19,20,33,35]],
    [["if",36,38],["block",37]],
    [["if",31,36],["block",37,63,64,65,66]],
    [["if",22,36],["block",63,64,65,66]],
    [["if",27,36],["block",63,64,65,66]],
    [["if",25,36],["block",63,64,65,66]],
    [["if",36,40],["block",1]],
    [["if",36,41,42],["block",1]],
    [["if",36,39],["block",1]],
    [["if",36,37],["block",1]]]
},
"runtime":[
[],[]
]


};
var aa,ca=this,da=/^[\w+/_-]+[=]{0,2}$/,ea=null;var fa=function(){},ha=function(a){return"function"==typeof a},ia=function(a){return"string"==typeof a},ja=function(a){return"number"==typeof a&&!isNaN(a)},ka=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},la=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},ma=function(a,b){if(a&&ka(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},oa=function(a,b){if(!ja(a)||
!ja(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},pa=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},qa=function(a){return Math.round(Number(a))||0},ra=function(a){return"false"==String(a).toLowerCase()?!1:!!a},sa=function(a){var b=[];if(ka(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},ta=function(a){return a?a.replace(/^\s+|\s+$/g,""):""},ua=function(){return(new Date).getTime()},va=function(){this.prefix="gtm.";this.values=
{}};va.prototype.set=function(a,b){this.values[this.prefix+a]=b};va.prototype.get=function(a){return this.values[this.prefix+a]};va.prototype.contains=function(a){return void 0!==this.get(a)};
var wa=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},xa=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},ya=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},za=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Aa=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ba=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Ca=function(a){if(null==a)return String(a);var b=Ba.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Da=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ea=function(a){if(!a||"object"!=Ca(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Da(a,"constructor")&&!Da(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Da(a,b)},Fa=function(a,b){var c=b||("array"==Ca(a)?[]:{}),d;for(d in a)if(Da(a,d)){var e=a[d];"array"==Ca(e)?("array"!=Ca(c[d])&&(c[d]=[]),c[d]=Fa(e,c[d])):Ea(e)?(Ea(c[d])||(c[d]={}),c[d]=Fa(e,c[d])):c[d]=e}return c};var f=window,u=document,Ga=navigator,Ha=u.currentScript&&u.currentScript.src,Ia=function(a,b){var c=f[a];f[a]=void 0===c?b:c;return f[a]},Ja=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},Ka=function(a,b,c){var d=u.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;Ja(d,b);c&&(d.onerror=c);var e;if(null===ea)b:{var g=ca.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&da.test(k)){ea=k;break b}}ea=""}e=ea;e&&d.setAttribute("nonce",e);var l=u.getElementsByTagName("script")[0]||u.body||u.head;l.parentNode.insertBefore(d,l);return d},La=function(){if(Ha){var a=Ha.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},Ma=function(a,b){var c=u.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=u.body&&u.body.lastChild||
u.body||u.head;d.parentNode.insertBefore(c,d);Ja(c,b);void 0!==a&&(c.src=a);return c},Na=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},Oa=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},Pa=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},z=function(a){f.setTimeout(a,0)},Ra=function(a){var b=
u.getElementById(a);if(b&&Qa(b,"id")!=a)for(var c=1;c<document.all[a].length;c++)if(Qa(document.all[a][c],"id")==a)return document.all[a][c];return b},Qa=function(a,b){return a&&b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},Sa=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},Ta=function(a){var b=u.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=
[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},Ua=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;g=g.parentElement}return null};var Va=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var Xa=/:[0-9]+$/,Ya=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},ab=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=Za(a.protocol)||Za(f.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:f.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||f.location.hostname).replace(Xa,"").toLowerCase());var g=b,h,k=Za(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=$a(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(Xa,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":h="/"==a.pathname.substr(0,1)?a.pathname:"/"+a.pathname;var m=h.split("/");0<=
la(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=Ya(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},Za=function(a){return a?a.replace(":","").toLowerCase():""},$a=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},bb=function(a){var b=u.createElement("a");
a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(c="/"+c);var d=b.hostname.replace(Xa,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};var cb=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},fb=function(a,b,c,d){var e=db(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=eb(e,function(g){return g.yb},b);if(1===e.length)return e[0].id;e=eb(e,function(g){return g.Sa},c);return e[0]?e[0].id:void 0}};
function hb(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=cb(b,e).indexOf(c)}
var kb=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var p=void 0,t=void 0,q;for(q in h)if(h.hasOwnProperty(q)){var r=h[q];if(null!=r)switch(q){case "secure":r&&(m+="; secure");break;case "domain":p=r;break;default:"path"==q&&(t=r),"expires"==q&&r instanceof Date&&(r=
r.toUTCString()),m+="; "+q+"="+r}}if("auto"===p){for(var v=ib(),x=0;x<v.length;++x){var y="none"!=v[x]?v[x]:void 0;if(!jb(y,t)&&hb(m+(y?"; domain="+y:""),a,l)){k=!0;break a}}k=!1}else p&&"none"!=p&&(m+="; domain="+p),k=!jb(p,t)&&hb(m,a,l)}return k};function eb(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function db(a,b){for(var c=[],d=cb(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),yb:1*k[0]||1,Sa:1*k[1]||1}))}}return c}
var lb=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,mb=/(^|\.)doubleclick\.net$/i,jb=function(a,b){return mb.test(document.location.hostname)||"/"===b&&lb.test(a)},ib=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));a.push("none");return a};
var nb=[],ob={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},pb=function(a){return ob[a]},rb=/[\x00\x22\x26\x27\x3c\x3e]/g;var vb=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,wb={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},xb=function(a){return wb[a]};nb[7]=function(a){return String(a).replace(vb,xb)};
nb[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(vb,xb)+"'"}};var Eb=/['()]/g,Fb=function(a){return"%"+a.charCodeAt(0).toString(16)};nb[12]=function(a){var b=
encodeURIComponent(String(a));Eb.lastIndex=0;return Eb.test(b)?b.replace(Eb,Fb):b};var Gb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,Hb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},Ib=function(a){return Hb[a]};nb[16]=function(a){return a};var Kb=[],Lb=[],Mb=[],Nb=[],Ob=[],Pb={},Qb,Rb,Sb,Tb=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},Ub=function(a){var b=a["function"];if(!b)throw Error("Error: No function name given for function call.");var c=!!Pb[b],d={},e;for(e in a)a.hasOwnProperty(e)&&0===e.indexOf("vtp_")&&(d[c?e:e.substr(4)]=a[e]);return c?Pb[b](d):(void 0)(b,d)},Wb=function(a,b,c,d){c=c||[];d=d||fa;var e={},g;for(g in a)a.hasOwnProperty(g)&&(e[g]=Vb(a[g],b,c,d));
return e},Xb=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=Pb[b];return c?c.priorityOverride||0:0},Vb=function(a,b,c,d){if(ka(a)){var e;switch(a[0]){case "function_id":return a[1];case "list":e=[];for(var g=1;g<a.length;g++)e.push(Vb(a[g],b,c,d));return e;case "macro":var h=a[1];if(c[h])return;var k=Kb[h];if(!k||b(k))return;c[h]=!0;try{var l=Wb(k,b,c,d);e=Ub(l);Sb&&(e=Sb.ff(e,l))}catch(y){d(y,h),e=!1}c[h]=!1;return e;case "map":e={};for(var m=
1;m<a.length;m+=2)e[Vb(a[m],b,c,d)]=Vb(a[m+1],b,c,d);return e;case "template":e=[];for(var n=!1,p=1;p<a.length;p++){var t=Vb(a[p],b,c,d);Rb&&(n=n||t===Rb.ob);e.push(t)}return Rb&&n?Rb.kf(e):e.join("");case "escape":e=Vb(a[1],b,c,d);if(Rb&&ka(a[1])&&"macro"===a[1][0]&&Rb.Nf(a))return Rb.Xf(e);e=String(e);for(var q=2;q<a.length;q++)nb[a[q]]&&(e=nb[a[q]](e));return e;case "tag":var r=a[1];if(!Nb[r])throw Error("Unable to resolve tag reference "+r+".");return e={wd:a[2],index:r};case "zb":var v={arg0:a[2],
arg1:a[3],ignore_case:a[5]};v["function"]=a[1];var x=Yb(v,b,c,d);a[4]&&(x=!x);return x;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},Yb=function(a,b,c,d){try{return Qb(Wb(a,b,c,d))}catch(e){JSON.stringify(a)}return null};var Zb=function(){var a=function(b){return{toString:function(){return b}}};return{Uc:a("convert_case_to"),Vc:a("convert_false_to"),Wc:a("convert_null_to"),Xc:a("convert_true_to"),Yc:a("convert_undefined_to"),ra:a("function"),ye:a("instance_name"),ze:a("live_only"),Ae:a("malware_disabled"),Bg:a("original_vendor_template_id"),Be:a("once_per_event"),md:a("once_per_load"),nd:a("setup_tags"),Ce:a("tag_id"),od:a("teardown_tags")}}();var $b=null,cc=function(a,b){function c(t){for(var q=0;q<t.length;q++)e[t[q]]=!0}var d=[],e=[];$b=ac(a,b||function(){});for(var g=0;g<Lb.length;g++){var h=Lb[g],k=bc(h);if(k){for(var l=h.add||[],m=0;m<l.length;m++)d[l[m]]=!0;c(h.block||[])}else null===k&&c(h.block||[])}for(var n=[],p=0;p<Nb.length;p++)d[p]&&!e[p]&&(n[p]=!0);return n},bc=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=$b(b[c]);if(!d)return null===d?null:!1}for(var e=a.unless||[],g=0;g<e.length;g++){var h=$b(e[g]);if(null===
h)return null;if(h)return!1}return!0},ac=function(a,b){var c=[];return function(d){void 0===c[d]&&(c[d]=Yb(Mb[d],a,void 0,b));return c[d]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */

var dc,ec=function(){};(function(){function a(k,l){k=k||"";l=l||{};for(var m in b)b.hasOwnProperty(m)&&(l.Se&&(l["fix_"+m]=!0),l.xd=l.xd||l["fix_"+m]);var n={comment:/^\x3c!--/,endTag:/^<\//,atomicTag:/^<\s*(script|style|noscript|iframe|textarea)[\s\/>]/i,startTag:/^</,chars:/^[^<]/},p={comment:function(){var q=k.indexOf("--\x3e");if(0<=q)return{content:k.substr(4,q),length:q+3}},endTag:function(){var q=k.match(d);if(q)return{tagName:q[1],length:q[0].length}},atomicTag:function(){var q=p.startTag();
if(q){var r=k.slice(q.length);if(r.match(new RegExp("</\\s*"+q.tagName+"\\s*>","i"))){var v=r.match(new RegExp("([\\s\\S]*?)</\\s*"+q.tagName+"\\s*>","i"));if(v)return{tagName:q.tagName,D:q.D,content:v[1],length:v[0].length+q.length}}}},startTag:function(){var q=k.match(c);if(q){var r={};q[2].replace(e,function(v,x,y,w,B){var A=y||w||B||g.test(x)&&x||null,C=document.createElement("div");C.innerHTML=A;r[x]=C.textContent||C.innerText||A});return{tagName:q[1],D:r,Ya:!!q[3],length:q[0].length}}},chars:function(){var q=
k.indexOf("<");return{length:0<=q?q:k.length}}},t=function(){for(var q in n)if(n[q].test(k)){var r=p[q]();return r?(r.type=r.type||q,r.text=k.substr(0,r.length),k=k.slice(r.length),r):null}};l.xd&&function(){var q=/^(AREA|BASE|BASEFONT|BR|COL|FRAME|HR|IMG|INPUT|ISINDEX|LINK|META|PARAM|EMBED)$/i,r=/^(COLGROUP|DD|DT|LI|OPTIONS|P|TD|TFOOT|TH|THEAD|TR)$/i,v=[];v.Id=function(){return this[this.length-1]};v.wc=function(C){var D=this.Id();return D&&D.tagName&&D.tagName.toUpperCase()===C.toUpperCase()};v.ef=
function(C){for(var D=0,H;H=this[D];D++)if(H.tagName===C)return!0;return!1};var x=function(C){C&&"startTag"===C.type&&(C.Ya=q.test(C.tagName)||C.Ya);return C},y=t,w=function(){k="</"+v.pop().tagName+">"+k},B={startTag:function(C){var D=C.tagName;"TR"===D.toUpperCase()&&v.wc("TABLE")?(k="<TBODY>"+k,A()):l.Jg&&r.test(D)&&v.ef(D)?v.wc(D)?w():(k="</"+C.tagName+">"+k,A()):C.Ya||v.push(C)},endTag:function(C){v.Id()?l.vf&&!v.wc(C.tagName)?w():v.pop():l.vf&&(y(),A())}},A=function(){var C=k,D=x(y());k=C;if(D&&
B[D.type])B[D.type](D)};t=function(){A();return x(y())}}();return{append:function(q){k+=q},dg:t,Pg:function(q){for(var r;(r=t())&&(!q[r.type]||!1!==q[r.type](r)););},clear:function(){var q=k;k="";return q},Qg:function(){return k},stack:[]}}var b=function(){var k={},l=this.document.createElement("div");l.innerHTML="<P><I></P></I>";k.Wg="<P><I></P></I>"!==l.innerHTML;l.innerHTML="<P><i><P></P></i></P>";k.Sg=2===l.childNodes.length;return k}(),c=/^<([\-A-Za-z0-9_]+)((?:\s+[\w\-]+(?:\s*=?\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
d=/^<\/([\-A-Za-z0-9_]+)[^>]*>/,e=/([\-A-Za-z0-9_]+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g,g=/^(checked|compact|declare|defer|disabled|ismap|multiple|nohref|noresize|noshade|nowrap|readonly|selected)$/i;a.supports=b;a.Xg=function(k){var l={comment:function(m){return"<--"+m.content+"--\x3e"},endTag:function(m){return"</"+m.tagName+">"},atomicTag:function(m){return l.startTag(m)+m.content+l.endTag(m)},startTag:function(m){var n="<"+m.tagName,p;for(p in m.D){var t=m.D[p];
n+=" "+p+'="'+(t?t.replace(/(^|[^\\])"/g,'$1\\"'):"")+'"'}return n+(m.Ya?"/>":">")},chars:function(m){return m.text}};return l[k.type](k)};a.Ig=function(k){var l={},m;for(m in k){var n=k[m];l[m]=n&&n.replace(/(^|[^\\])"/g,'$1\\"')}return l};for(var h in b)a.Xe=a.Xe||!b[h]&&h;dc=a})();(function(){function a(){}function b(p){return void 0!==p&&null!==p}function c(p,t,q){var r,v=p&&p.length||0;for(r=0;r<v;r++)t.call(q,p[r],r)}function d(p,t,q){for(var r in p)p.hasOwnProperty(r)&&t.call(q,r,p[r])}function e(p,
t){d(t,function(q,r){p[q]=r});return p}function g(p,t){p=p||{};d(t,function(q,r){b(p[q])||(p[q]=r)});return p}function h(p){try{return m.call(p)}catch(q){var t=[];c(p,function(r){t.push(r)});return t}}var k={Ie:a,Je:a,Ke:a,Le:a,Te:a,Ue:function(p){return p},done:a,error:function(p){throw p;},hg:!1},l=this;if(!l.postscribe){var m=Array.prototype.slice,n=function(){function p(q,r,v){var x="data-ps-"+r;if(2===arguments.length){var y=q.getAttribute(x);return b(y)?String(y):y}b(v)&&""!==v?q.setAttribute(x,
v):q.removeAttribute(x)}function t(q,r){var v=q.ownerDocument;e(this,{root:q,options:r,Za:v.defaultView||v.parentWindow,xa:v,Db:dc("",{Se:!0}),bc:[q],Ec:"",Fc:v.createElement(q.nodeName),Wa:[],la:[]});p(this.Fc,"proxyof",0)}t.prototype.write=function(){[].push.apply(this.la,arguments);for(var q;!this.xb&&this.la.length;)q=this.la.shift(),"function"===typeof q?this.af(q):this.Qc(q)};t.prototype.af=function(q){var r={type:"function",value:q.name||q.toString()};this.Bc(r);q.call(this.Za,this.xa);this.Md(r)};
t.prototype.Qc=function(q){this.Db.append(q);for(var r,v=[],x,y;(r=this.Db.dg())&&!(x=r&&"tagName"in r?!!~r.tagName.toLowerCase().indexOf("script"):!1)&&!(y=r&&"tagName"in r?!!~r.tagName.toLowerCase().indexOf("style"):!1);)v.push(r);this.yg(v);x&&this.Bf(r);y&&this.Cf(r)};t.prototype.yg=function(q){var r=this.Ye(q);r.qd&&(r.nc=this.Ec+r.qd,this.Ec+=r.bg,this.Fc.innerHTML=r.nc,this.vg())};t.prototype.Ye=function(q){var r=this.bc.length,v=[],x=[],y=[];c(q,function(w){v.push(w.text);if(w.D){if(!/^noscript$/i.test(w.tagName)){var B=
r++;x.push(w.text.replace(/(\/?>)/," data-ps-id="+B+" $1"));"ps-script"!==w.D.id&&"ps-style"!==w.D.id&&y.push("atomicTag"===w.type?"":"<"+w.tagName+" data-ps-proxyof="+B+(w.Ya?" />":">"))}}else x.push(w.text),y.push("endTag"===w.type?w.text:"")});return{Yg:q,raw:v.join(""),qd:x.join(""),bg:y.join("")}};t.prototype.vg=function(){for(var q,r=[this.Fc];b(q=r.shift());){var v=1===q.nodeType;if(!v||!p(q,"proxyof")){v&&(this.bc[p(q,"id")]=q,p(q,"id",null));var x=q.parentNode&&p(q.parentNode,"proxyof");
x&&this.bc[x].appendChild(q)}r.unshift.apply(r,h(q.childNodes))}};t.prototype.Bf=function(q){var r=this.Db.clear();r&&this.la.unshift(r);q.src=q.D.src||q.D.Cg;q.src&&this.Wa.length?this.xb=q:this.Bc(q);var v=this;this.xg(q,function(){v.Md(q)})};t.prototype.Cf=function(q){var r=this.Db.clear();r&&this.la.unshift(r);q.type=q.D.type||q.D.Dg||"text/css";this.zg(q);r&&this.write()};t.prototype.zg=function(q){var r=this.$e(q);this.Lf(r);q.content&&(r.styleSheet&&!r.sheet?r.styleSheet.cssText=q.content:
r.appendChild(this.xa.createTextNode(q.content)))};t.prototype.$e=function(q){var r=this.xa.createElement(q.tagName);r.setAttribute("type",q.type);d(q.D,function(v,x){r.setAttribute(v,x)});return r};t.prototype.Lf=function(q){this.Qc('<span id="ps-style"/>');var r=this.xa.getElementById("ps-style");r.parentNode.replaceChild(q,r)};t.prototype.Bc=function(q){q.Tf=this.la;this.la=[];this.Wa.unshift(q)};t.prototype.Md=function(q){q!==this.Wa[0]?this.options.error({message:"Bad script nesting or script finished twice"}):
(this.Wa.shift(),this.write.apply(this,q.Tf),!this.Wa.length&&this.xb&&(this.Bc(this.xb),this.xb=null))};t.prototype.xg=function(q,r){var v=this.Ze(q),x=this.og(v),y=this.options.Ie;q.src&&(v.src=q.src,this.lg(v,x?y:function(){r();y()}));try{this.Kf(v),q.src&&!x||r()}catch(w){this.options.error(w),r()}};t.prototype.Ze=function(q){var r=this.xa.createElement(q.tagName);d(q.D,function(v,x){r.setAttribute(v,x)});q.content&&(r.text=q.content);return r};t.prototype.Kf=function(q){this.Qc('<span id="ps-script"/>');
var r=this.xa.getElementById("ps-script");r.parentNode.replaceChild(q,r)};t.prototype.lg=function(q,r){function v(){q=q.onload=q.onreadystatechange=q.onerror=null}var x=this.options.error;e(q,{onload:function(){v();r()},onreadystatechange:function(){/^(loaded|complete)$/.test(q.readyState)&&(v(),r())},onerror:function(){var y={message:"remote script failed "+q.src};v();x(y);r()}})};t.prototype.og=function(q){return!/^script$/i.test(q.nodeName)||!!(this.options.hg&&q.src&&q.hasAttribute("async"))};
return t}();l.postscribe=function(){function p(){var x=r.shift(),y;x&&(y=x[x.length-1],y.Je(),x.stream=t.apply(null,x),y.Ke())}function t(x,y,w){function B(H){H=w.Ue(H);v.write(H);w.Le(H)}v=new n(x,w);v.id=q++;v.name=w.name||v.id;var A=x.ownerDocument,C={close:A.close,open:A.open,write:A.write,writeln:A.writeln};e(A,{close:a,open:a,write:function(){return B(h(arguments).join(""))},writeln:function(){return B(h(arguments).join("")+"\n")}});var D=v.Za.onerror||a;v.Za.onerror=function(H,N,R){w.error({Mg:H+
" - "+N+":"+R});D.apply(v.Za,arguments)};v.write(y,function(){e(A,C);v.Za.onerror=D;w.done();v=null;p()});return v}var q=0,r=[],v=null;return e(function(x,y,w){"function"===typeof w&&(w={done:w});w=g(w,k);x=/^#/.test(x)?l.document.getElementById(x.substr(1)):x.Kg?x[0]:x;var B=[x,y,w];x.Wf={cancel:function(){B.stream?B.stream.abort():B[1]=a}};w.Te(B);r.push(B);v||p();return x.Wf},{streams:{},Og:r,Eg:n})}();ec=l.postscribe}})();var rc={},sc=null,tc=Math.random();rc.m="GTM-5B3BRS";rc.sb="430";var uc="www.googletagmanager.com/gtm.js";var vc=uc,wc=null,xc=null,yc=null,zc="//www.googletagmanager.com/a?id="+rc.m+"&cv=152",Ac={},Bc={},Cc=function(){var a=sc.sequence||0;sc.sequence=a+1;return a};var E=function(a,b,c,d){return(2===Dc()||d||"http:"!=f.location.protocol?a:b)+c},Dc=function(){var a=La(),b;if(1===a)a:{var c=vc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=u.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var Ec=!1;var Ic={},Jc=function(a){Ic.GTM=Ic.GTM||[];Ic.GTM[a]=!0};
var Kc=function(){return"&tc="+Nb.filter(function(a){return a}).length},Tc=function(){Lc&&(f.clearTimeout(Lc),Lc=void 0);void 0===Mc||Nc[Mc]&&!Oc||(Pc[Mc]||Qc.Pf()||0>=Rc--?(Jc(1),Pc[Mc]=!0):(Qc.fg(),Na(Sc()),Nc[Mc]=!0,Oc=""))},Sc=function(){var a=Mc;if(void 0===a)return"";for(var b,c=[],d=Ic.GTM||[],e=0;e<d.length;e++)d[e]&&(c[Math.floor(e/6)]^=1<<e%6);for(var g=0;g<c.length;g++)c[g]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(c[g]||0);b=c.join("");return[Uc,Nc[a]?"":
"&es=1",Vc[a],b?"&u="+b:"",Kc(),Oc,"&z=0"].join("")},Wc=function(){return[zc,"&v=3&t=t","&pid="+oa(),"&rv="+rc.sb].join("")},Xc="0.005000">Math.random(),Uc=Wc(),Yc=function(){Uc=Wc()},Nc={},Oc="",Mc=void 0,Vc={},Pc={},Lc=void 0,Qc=function(a,b){var c=0,d=0;return{Pf:function(){if(c<a)return!1;ua()-d>=b&&(c=0);return c>=a},fg:function(){ua()-d>=b&&(c=0);c++;d=ua()}}}(2,1E3),Rc=1E3,Zc=function(a,b){if(Xc&&!Pc[a]&&Mc!==a){Tc();Mc=a;Oc="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):
"*";Vc[a]="&e="+c+"&eid="+a;Lc||(Lc=f.setTimeout(Tc,500))}},$c=function(a,b,c){if(Xc&&!Pc[a]&&b){a!==Mc&&(Tc(),Mc=a);var d=c+String(b[Zb.ra]||"").replace(/_/g,"");Oc=Oc?Oc+"."+d:"&tr="+d;Lc||(Lc=f.setTimeout(Tc,500));2022<=Sc().length&&Tc()}};var ad=new va,bd={},cd={},gd={name:"dataLayer",set:function(a,b){Fa(dd(a,b),bd);ed()},get:function(a){return fd(a,2)},reset:function(){ad=new va;bd={};ed()}},fd=function(a,b){if(2!=b){var c=ad.get(a);if(Xc){var d=hd(a);c!==d&&Jc(5)}return c}return hd(a)},hd=function(a,b,c){var d=a.split("."),e=!1,g=void 0;return e?g:jd(d)},jd=function(a){for(var b=bd,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var md=function(a,b){cd.hasOwnProperty(a)||(ad.set(a,b),Fa(dd(a,b),bd),ed())},dd=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c},ed=function(a){pa(cd,function(b,c){ad.set(b,c);Fa(dd(b,void 0),bd);Fa(dd(b,c),bd);a&&delete cd[b]})};var nd=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),od={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},pd={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]};
var rd=function(a){var b=fd("gtm.whitelist");b&&Jc(9);var c=b&&Aa(sa(b),od),d=fd("gtm.blacklist");d||(d=fd("tagTypeBlacklist"))&&Jc(3);d?Jc(8):d=[];
qd()&&(d=sa(d),d.push("nonGooglePixels","nonGoogleScripts"));0<=la(sa(d),"google")&&Jc(2);var e=d&&Aa(sa(d),pd),g={};return function(h){var k=h&&h[Zb.ra];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];var l=Bc[k]||[],m=a(k);if(b){var n;if(n=m)a:{if(0>la(c,k))if(l&&0<l.length)for(var p=0;p<l.length;p++){if(0>la(c,l[p])){Jc(11);
n=!1;break a}}else{n=!1;break a}n=!0}m=n}var t=!1;if(d){var q=0<=la(e,k);if(q)t=q;else{var r;b:{for(var v=l||[],x=new va,y=0;y<e.length;y++)x.set(e[y],!0);for(var w=0;w<v.length;w++)if(x.get(v[w])){r=!0;break b}r=!1}var B=r;B&&Jc(10);t=B}}return g[k]=!m||t}},qd=function(){return nd.test(f.location&&f.location.hostname)};var sd={ff:function(a,b){b[Zb.Uc]&&"string"===typeof a&&(a=1==b[Zb.Uc]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(Zb.Wc)&&null===a&&(a=b[Zb.Wc]);b.hasOwnProperty(Zb.Yc)&&void 0===a&&(a=b[Zb.Yc]);b.hasOwnProperty(Zb.Xc)&&!0===a&&(a=b[Zb.Xc]);b.hasOwnProperty(Zb.Vc)&&!1===a&&(a=b[Zb.Vc]);return a}};var td={active:!0,isWhitelisted:function(){return!0}},ud=function(a){var b=sc.zones;!b&&a&&(b=sc.zones=a());return b};var vd=!1,wd=0,xd=[];function yd(a){if(!vd){var b=u.createEventObject,c="complete"==u.readyState,d="interactive"==u.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){vd=!0;for(var e=0;e<xd.length;e++)z(xd[e])}xd.push=function(){for(var g=0;g<arguments.length;g++)z(arguments[g]);return 0}}}function zd(){if(!vd&&140>wd){wd++;try{u.documentElement.doScroll("left"),yd()}catch(a){f.setTimeout(zd,50)}}}var Ad=function(a){vd?a():xd.push(a)};var Bd=function(){function a(d){return!ja(d)||0>d?0:d}if(!sc._li&&f.performance&&f.performance.timing){var b=f.performance.timing.navigationStart,c=ja(gd.get("gtm.start"))?gd.get("gtm.start"):0;sc._li={cst:a(c-b),cbt:a(xc-b)}}};var Fd=!1,Gd=function(){return f.GoogleAnalyticsObject&&f[f.GoogleAnalyticsObject]},Hd=!1;
var Id=function(a){f.GoogleAnalyticsObject||(f.GoogleAnalyticsObject=a||"ga");var b=f.GoogleAnalyticsObject;if(f[b])f.hasOwnProperty(b)||Jc(12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);f[b]=c}Bd();return f[b]},Jd=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=Gd();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var Ld=function(){},Kd=function(){return f.GoogleAnalyticsObject||"ga"},Md=!1;var Td=function(a){};function Sd(a,b){a.containerId=rc.m;var c={type:"GENERIC",value:a};b.length&&(c.trace=b);return c};function Ud(a,b,c,d,e){var g=Nb[a],h=Vd(a,b,c,d,e);if(!h)return null;var k=Vb(g[Zb.nd],d.fa,[],fa);if(k&&k.length){var l=k[0];h=Ud(l.index,b,{I:h,O:1===l.wd?c.terminate:h,terminate:c.terminate},d,e)}return h}
function Vd(a,b,c,d,e){function g(){if(h[Zb.Ae])l();else{var y=Wb(h,d.fa,[],function(B){Jc(6);Td(B)}),w=!1;y.vtp_gtmOnSuccess=function(){if(!w){w=!0;$c(d.id,Nb[a],"5");k()}};y.vtp_gtmOnFailure=function(){if(!w){w=!0;$c(d.id,Nb[a],"6");l()}};y.vtp_gtmTagId=h.tag_id;$c(d.id,h,"1");try{Ub(y)}catch(B){Td(B);$c(d.id,h,"7");w||(w=!0,l())}}}var h=Nb[a],k=c.I,l=c.O,m=c.terminate;if(d.fa(h))return null;var n=Vb(h[Zb.od],d.fa,[],fa);if(n&&n.length){var p=n[0],t=Ud(p.index,b,{I:k,O:l,terminate:m},d,e);if(!t)return null;
k=t;l=2===p.wd?m:t}if(h[Zb.md]||h[Zb.Be]){var q=h[Zb.md]?Ob:b,r=k,v=l;if(!q[a]){g=xa(g);var x=Wd(a,q,g);k=x.I;l=x.O}return function(){q[a](r,v)}}return g}function Wd(a,b,c){var d=[],e=[];b[a]=Xd(d,e,c);return{I:function(){b[a]=Yd;for(var g=0;g<d.length;g++)d[g]()},O:function(){b[a]=Zd;for(var g=0;g<e.length;g++)e[g]()}}}function Xd(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function Yd(a){a()}function Zd(a,b){b()};function $d(a){var b=0,c=0,d=!1;return{add:function(){c++;return xa(function(){b++;d&&b>=c&&a()})},Qe:function(){d=!0;b>=c&&a()}}}var ce=function(a){for(var b=$d(a.callback),c=[],d=[],e=0;e<Nb.length;e++)if(a.Ua[e]){var g=Nb[e];var h=b.add();try{var k=Ud(e,c,{I:h,O:h,terminate:h},a,e);k?d.push({Wd:e,b:Xb(g),uf:k}):(ae(e,a),h())}catch(m){h()}}b.Qe();d.sort(be);for(var l=0;l<d.length;l++)d[l].uf();return 0<d.length};
function be(a,b){var c,d=b.b,e=a.b;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.Wd,k=b.Wd;g=h>k?1:h<k?-1:0}return g}function ae(a,b){if(!Xc)return;var c=function(d){var e=b.fa(Nb[d])?"3":"4",g=Vb(Nb[d][Zb.nd],b.fa,[],fa);g&&g.length&&c(g[0].index);$c(b.id,Nb[d],e);var h=Vb(Nb[d][Zb.od],b.fa,[],fa);h&&h.length&&c(h[0].index)};c(a);}
var de=!1,ee=function(a,b,c,d){if("gtm.js"==b){if(de)return!1;de=!0}Zc(a,b);var e=rd(c),g={id:a,name:b,callback:d||fa,fa:e,Ua:[]};g.Ua=cc(e,function(n){Td(n)});var h=ce(g);"gtm.js"!==b&&"gtm.sync"!==b||Ld();if(!h)return h;for(var k={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0},l=0;l<
g.Ua.length;l++)if(g.Ua[l]){var m=Nb[l];if(m&&!k[m[Zb.ra]])return!0}return!1};var F={Ob:"event_callback",Qb:"event_timeout"};var ge={};var he=/[A-Z]+/,ie=/\s/,je=function(a){if(ia(a)&&(a=ta(a),!ie.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(he.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],da:d}}}}},le=function(a){for(var b={},c=0;c<a.length;++c){var d=je(a[c]);d&&(b[d.id]=d)}ke(b);var e=[];pa(b,function(g,h){e.push(h)});return e};
function ke(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.da[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var me=null,ne={},oe={},qe,re=function(a,b){var c={event:a};b&&(c.eventModel=Fa(b),b[F.Ob]&&(c.eventCallback=b[F.Ob]),b[F.Qb]&&(c.eventTimeout=b[F.Qb]));return c};
var we={config:function(a){},event:function(a){var b=a[1];if(ia(b)&&!(3<a.length)){var c;if(2<
a.length){if(!Ea(a[2]))return;c=a[2]}var d=re(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){if(3===a.length){var b=a[1],c=a[2];ge[b]||(ge[b]=[]);ge[b].push(c)}},set:function(a){var b;2==a.length&&Ea(a[1])?b=Fa(a[1]):3==a.length&&ia(a[1])&&(b={},b[a[1]]=a[2]);if(b)return b.eventModel=Fa(b),b.event="gtag.set",b._clear=!0,b}},xe={policy:!0};var ye=function(){return!1};var Ee=function(a){if(De(a))return a;this.ug=a};Ee.prototype.Af=function(){return this.ug};var De=function(a){return!a||"object"!==Ca(a)||Ea(a)?!1:"getUntrustedUpdateValue"in a};Ee.prototype.getUntrustedUpdateValue=Ee.prototype.Af;var Fe=!1,Ge=[];function He(){if(!Fe){Fe=!0;for(var a=0;a<Ge.length;a++)z(Ge[a])}}var Ie=function(a){Fe?z(a):Ge.push(a)};var Je=[],Ke=!1;function Le(a){var b=a.eventCallback,c=xa(function(){ha(b)&&z(function(){b(rc.m)})}),d=a.eventTimeout;d&&f.setTimeout(c,Number(d));return c}
var Me=function(a){return f["dataLayer"].push(a)},Oe=function(a){var b=a._clear;pa(a,function(g,h){"_clear"!==g&&(b&&md(g,void 0),md(g,h))});var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=Cc(),a["gtm.uniqueEventId"]=d,md("gtm.uniqueEventId",d));yc=c;var e=Ne(a);yc=null;if(!wc){wc=a["gtm.start"];}return e};
function Ne(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=sc.zones;d=e?e.checkState(rc.m,c):td;if(!d.active)return!1;var g=Le(a);return ee(c,b,d.isWhitelisted,g)?!0:!1}
var Pe=function(){for(var a=!1;!Ke&&0<Je.length;){Ke=!0;delete bd.eventModel;ed();var b=Je.shift();if(null!=b){var c=De(b);if(c){var d=b;b=De(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=fd(h,1);if(ka(k)||Ea(k))k=Fa(k);cd[h]=k}}try{if(ha(b))try{b.call(gd)}catch(v){}else if(ka(b)){var l=b;if(ia(l[0])){var m=
l[0].split("."),n=m.pop(),p=l.slice(1),t=fd(m.join("."),2);if(void 0!==t&&null!==t)try{t[n].apply(t,p)}catch(v){}}}else{var q=b;if(q&&("[object Arguments]"==Object.prototype.toString.call(q)||Object.prototype.hasOwnProperty.call(q,"callee"))){a:{if(b.length&&ia(b[0])){var r=we[b[0]];if(r&&(!c||!xe[b[0]])){b=r(b);break a}}b=void 0}if(!b){Ke=!1;continue}}a=Oe(b)||a}}finally{c&&ed(!0)}}Ke=!1}
return!a},Qe=function(){var a=Pe();try{var b=rc.m,c=f["dataLayer"].hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}}catch(g){}return a},Re=function(){var a=Ia("dataLayer",[]),b=Ia("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};Ad(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});Ie(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});var c=a.push;a.push=function(){var d;
if(0<sc.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new Ee(arguments[e])}else d=[].slice.call(arguments,0);var g=c.apply(a,d);Je.push.apply(Je,d);if(300<this.length)for(Jc(4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return Pe()&&h};Je.push.apply(Je,a.slice(0));z(Qe)};var Te=function(a){return Se?u.querySelectorAll(a):null},Ue=function(a,b){if(!Se)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!u.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},Ve=!1;if(u.querySelectorAll)try{var We=u.querySelectorAll(":root");We&&1==We.length&&We[0]==u.documentElement&&(Ve=!0)}catch(a){}var Se=Ve;var Xe;var tf={};tf.ob=new String("undefined");
var uf=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===tf.ob?b:a[d]);return c.join("")}};uf.prototype.toString=function(){return this.resolve("undefined")};uf.prototype.valueOf=uf.prototype.toString;tf.De=uf;tf.$b={};tf.kf=function(a){return new uf(a)};var vf={};tf.gg=function(a,b){var c=Cc();vf[c]=[a,b];return c};tf.td=function(a){var b=a?0:1;return function(c){var d=vf[c];if(d&&"function"===typeof d[b])d[b]();vf[c]=void 0}};tf.Nf=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};tf.Xf=function(a){if(a===tf.ob)return a;var b=Cc();tf.$b[b]=a;return'google_tag_manager["'+rc.m+'"].macro('+b+")"};tf.Rf=function(a,b,c){a instanceof tf.De&&(a=a.resolve(tf.gg(b,c)),b=fa);return{nc:a,I:b}};var wf=function(a,b,c){var d={event:b,"gtm.element":a,"gtm.elementClasses":a.className,"gtm.elementId":a["for"]||Qa(a,"id")||"","gtm.elementTarget":a.formTarget||a.target||""};c&&(d["gtm.triggers"]=c.join(","));d["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||a.href||a.src||a.code||a.codebase||"";return d},xf=function(a){sc.hasOwnProperty("autoEventsSettings")||(sc.autoEventsSettings={});var b=sc.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},
yf=function(a,b,c){xf(a)[b]=c},zf=function(a,b,c,d){var e=xf(a),g=wa(e,b,d);e[b]=c(g)},Af=function(a,b,c){var d=xf(a);return wa(d,b,c)};var Bf=function(){for(var a=Ga.userAgent+(u.cookie||"")+(u.referrer||""),b=a.length,c=f.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(ua()/1E3)].join(".")},Ef=function(a,b,c,d){var e=Cf(b);return fb(a,e,Df(c),d)},Cf=function(a){if(!a)return 1;a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},Df=function(a){if(!a||
"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};function Ff(a,b){var c=""+Cf(a),d=Df(b);1<d&&(c+="-"+d);return c};var Gf=["1"],Hf={},Lf=function(a,b,c,d){var e=If(a);Hf[e]||Jf(e,b,c)||(Kf(e,Bf(),b,c,d),Jf(e,b,c))};function Kf(a,b,c,d,e){var g;g=["1",Ff(d,c),b].join(".");kb(a,g,c,d,0==e?void 0:new Date(ua()+1E3*(void 0==e?7776E3:e)))}function Jf(a,b,c){var d=Ef(a,b,c,Gf);d&&(Hf[a]=d);return d}function If(a){return(a||"_gcl")+"_au"};var Mf=function(){for(var a=[],b=u.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({Mc:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].Mc]||(g[a[h].Mc]=[]),g[a[h].Mc].push({timestamp:k[1],xf:k[2]}))}return g};function Nf(){for(var a=Of,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function Pf(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}
var Of,Qf,Rf=function(a){Of=Of||Pf();Qf=Qf||Nf();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,n=(h&15)<<2|k>>6,p=k&63;e||(p=64,d||(n=64));b.push(Of[l],Of[m],Of[n],Of[p])}return b.join("")},Sf=function(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=Qf[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}Of=Of||Pf();Qf=Qf||
Nf();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var Tf;function Uf(a,b){if(!a||b===u.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}var Vf=function(){var a=Ia("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var Wf=/(.*?)\*(.*?)\*(.*)/,Xf=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,Yf=/^(?:www\.|m\.|amp\.)+/,Zf=/([^?#]+)(\?[^#]*)?(#.*)?/,$f=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,bg=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(Rf(String(d))))}var e=b.join("*");return["1",ag(e),e].join("*")},ag=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=Tf)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}Tf=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^Tf[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},dg=function(){return function(a){var b=bb(f.location.href),c=b.search.replace("?",""),d=Ya(c,"_gl",!0)||"";a.query=cg(d)||{};var e=ab(b,"fragment").match($f);a.fragment=cg(e&&e[3]||
"")||{}}},cg=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=Wf.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===ag(k,n)){l=!0;break a}l=!1}if(l){for(var p={},t=k?k.split("*"):[],q=0;q<t.length;q+=2)p[t[q]]=Sf(t[q+1]);return p}}}}catch(r){}};
function eg(a,b,c){function d(m){var n=m,p=$f.exec(n),t=n;if(p){var q=p[2],r=p[4];t=p[1];r&&(t=t+q+r)}m=t;var v=m.charAt(m.length-1);m&&"&"!==v&&(m+="&");return m+l}c=void 0===c?!1:c;var e=Zf.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function fg(a,b,c){for(var d={},e={},g=Vf().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&Uf(k.domains,b)&&(k.fragment?ya(e,k.callback()):ya(d,k.callback()))}if(za(d)){var l=bg(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var n=a.childNodes||[],p=!1,t=0;t<n.length;t++){var q=n[t];if("_gl"===q.name){q.setAttribute("value",l);p=!0;break}}if(!p){var r=u.createElement("input");r.setAttribute("type","hidden");r.setAttribute("name","_gl");r.setAttribute("value",
l);a.appendChild(r)}}else if("post"===m){var v=eg(l,a.action);Va.test(v)&&(a.action=v)}}}else gg(l,a,!1)}if(!c&&za(e)){var x=bg(e);gg(x,a,!0)}}function gg(a,b,c){if(b.href){var d=eg(a,b.href,void 0===c?!1:c);Va.test(d)&&(b.href=d)}}
var hg=function(a){try{var b;a:{for(var c=a.target||a.srcElement||{},d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||fg(e,e.hostname,!1)}}catch(h){}},ig=function(a){try{var b=a.target||a.srcElement||{};if(b.action){var c=ab(bb(b.action),"host");fg(b,c,!0)}}catch(d){}},jg=function(a,b,c,d){var e=Vf();e.init||(Oa(u,"mousedown",hg),Oa(u,"keyup",hg),Oa(u,"submit",ig),e.init=!0);var g={callback:a,
domains:b,fragment:"fragment"===c,forms:!!d};Vf().decorators.push(g)},kg=function(){var a=u.location.hostname,b=Xf.exec(u.referrer);if(!b)return!1;var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}return a.replace(Yf,"")===e.replace(Yf,"")},lg=function(a,b){return!1===a?!1:a||b||kg()};var mg=/^\w+$/,ng=/^[\w-]+$/,og=/^~?[\w-]+$/,pg={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha"};function qg(a){return a&&"string"==typeof a&&a.match(mg)?a:"_gcl"}var sg=function(){var a=bb(f.location.href),b=ab(a,"query",!1,void 0,"gclid"),c=ab(a,"query",!1,void 0,"gclsrc"),d=ab(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||Ya(e,"gclid",void 0);c=c||Ya(e,"gclsrc",void 0)}return rg(b,c,d)};
function rg(a,b,c){var d={},e=function(g,h){d[h]||(d[h]=[]);d[h].push(g)};if(void 0!==a&&a.match(ng))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha")}c&&e(c,"dc");return d}
function tg(a,b,c){function d(p,t){var q=ug(p,e);q&&kb(q,t,h,g,l,!0)}b=b||{};var e=qg(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.Jd?7776E3:b.Jd;c=c||ua();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(p){return["GCL",m,p].join(".")};a.aw&&(!0===b.$g?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]))}
var ug=function(a,b){var c=pg[a];if(void 0!==c)return b+c},vg=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||0)};function wg(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var xg=function(a,b,c,d,e){if(ka(b)){var g=qg(e);jg(function(){for(var h={},k=0;k<a.length;++k){var l=ug(a[k],g);if(l){var m=cb(l,u.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},yg=function(a){return a.filter(function(b){return og.test(b)})},zg=function(a){for(var b=["aw","dc"],c=qg(a&&a.prefix),d={},e=0;e<b.length;e++)pg[b[e]]&&(d[b[e]]=pg[b[e]]);pa(d,function(g,h){var k=cb(c+h,u.cookie);if(k.length){var l=k[0],m=vg(l),n={};n[g]=[wg(l)];tg(n,a,m)}})};var Ag=/^\d+\.fls\.doubleclick\.net$/;function Bg(a){var b=bb(f.location.href),c=ab(b,"host",!1);if(c&&c.match(Ag)){var d=ab(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function Cg(a,b){if("aw"==a||"dc"==a){var c=Bg("gcl"+a);if(c)return c.split(".")}var d=qg(b);if("_gcl"==d){var e;e=sg()[a]||[];if(0<e.length)return e}var g=ug(a,d),h;if(g){var k=[];if(u.cookie){var l=cb(g,u.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=wg(l[m]);n&&-1===la(k,n)&&k.push(n)}h=yg(k)}else h=k}else h=k}else h=[];return h}
var Dg=function(){var a=Bg("gac");if(a)return decodeURIComponent(a);var b=Mf(),c=[];pa(b,function(d,e){for(var g=[],h=0;h<e.length;h++)g.push(e[h].xf);g=yg(g);g.length&&c.push(d+":"+g.join(","))});return c.join(";")},Eg=function(a,b,c,d,e){Lf(b,c,d,e);var g=Hf[If(b)],h=sg().dc||[],k=!1;if(g&&0<h.length){var l=sc.joined_au=sc.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var p="https://adservice.google.com/ddm/regclk",t=p=p+"?gclid="+h[n]+"&auiddc="+g;Ga.sendBeacon&&Ga.sendBeacon(t)||Na(t);k=l[m]=
!0}}null==a&&(a=k);if(a&&g){var q=If(b),r=Hf[q];r&&Kf(q,r,c,d,e)}};var Fg;if(3===rc.sb.length)Fg="g";else{var Gg="G";Fg=Gg}
var Hg={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:Fg},Ig=function(a){var b=rc.m.split("-"),c=b[0].toUpperCase(),d=Hg[c]||"i",e=a&&"GTM"===c?b[1]:"",g;if(3===rc.sb.length){var h=void 0;g="2"+(h||"w")}else g="";return g+d+rc.sb+e};var Pg=!!f.MutationObserver,Qg=void 0,Rg=function(a){if(!Qg){var b=function(){var c=u.body;if(c)if(Pg)(new MutationObserver(function(){for(var e=0;e<Qg.length;e++)z(Qg[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;Oa(c,"DOMNodeInserted",function(){d||(d=!0,z(function(){d=!1;for(var e=0;e<Qg.length;e++)z(Qg[e])}))})}};Qg=[];u.body?b():z(b)}Qg.push(a)};
var Zg=function(){var a=u.body,b=u.documentElement||a&&a.parentElement,c,d;if(u.compatMode&&"BackCompat"!==u.compatMode)c=b?b.clientHeight:0,d=b?b.clientWidth:0;else{var e=function(g,h){return g&&h?Math.min(g,h):Math.max(g,h)};Jc(7);c=e(b?b.clientHeight:0,a?a.clientHeight:0);d=e(b?b.clientWidth:0,a?a.clientWidth:0)}return{width:d,height:c}},$g=function(a){var b=Zg(),c=b.height,d=b.width,e=a.getBoundingClientRect(),g=e.bottom-e.top,h=e.right-e.left;return g&&h?(1-Math.min((Math.max(0-e.left,0)+Math.max(e.right-
d,0))/h,1))*(1-Math.min((Math.max(0-e.top,0)+Math.max(e.bottom-c,0))/g,1)):0},ah=function(a){if(u.hidden)return!0;var b=a.getBoundingClientRect();if(b.top==b.bottom||b.left==b.right||!f.getComputedStyle)return!0;var c=f.getComputedStyle(a,null);if("hidden"===c.visibility)return!0;for(var d=a,e=c;d;){if("none"===e.display)return!0;var g=e.opacity,h=e.filter;if(h){var k=h.indexOf("opacity(");0<=k&&(h=h.substring(k+8,h.indexOf(")",k)),"%"==h.charAt(h.length-1)&&(h=h.substring(0,h.length-1)),g=Math.min(h,
g))}if(void 0!==g&&0>=g)return!0;(d=d.parentElement)&&(e=f.getComputedStyle(d,null))}return!1};var bh=[],ch=!(!f.IntersectionObserver||!f.IntersectionObserverEntry),dh=function(a,b,c){for(var d=new f.IntersectionObserver(a,{threshold:c}),e=0;e<b.length;e++)d.observe(b[e]);for(var g=0;g<bh.length;g++)if(!bh[g])return bh[g]=d,g;return bh.push(d)-1},eh=function(a,b,c){function d(k,l){var m={top:0,bottom:0,right:0,left:0,width:0,height:0},n={boundingClientRect:k.getBoundingClientRect(),
intersectionRatio:l,intersectionRect:m,isIntersecting:0<l,rootBounds:m,target:k,time:ua()};z(function(){return a(n)})}for(var e=[],g=[],h=0;h<b.length;h++)e.push(0),g.push(-1);c.sort(function(k,l){return k-l});return function(){for(var k=0;k<b.length;k++){var l=$g(b[k]);if(l>e[k])for(;g[k]<c.length-1&&l>=c[g[k]+1];)d(b[k],l),g[k]++;else if(l<e[k])for(;0<=g[k]&&l<=c[g[k]];)d(b[k],l),g[k]--;e[k]=l}}},fh=function(a,b,c){for(var d=0;d<c.length;d++)1<c[d]?c[d]=1:0>c[d]&&(c[d]=0);if(ch){var e=!1;z(function(){e||
eh(a,b,c)()});return dh(function(g){e=!0;for(var h={Da:0};h.Da<g.length;h={Da:h.Da},h.Da++)z(function(k){return function(){return a(g[k.Da])}}(h))},b,c)}return f.setInterval(eh(a,b,c),1E3)},gh=function(a){ch?0<=a&&a<bh.length&&bh[a]&&(bh[a].disconnect(),bh[a]=void 0):f.clearInterval(a)};var ih=f.clearTimeout,jh=f.setTimeout,G=function(a,b,c){if(ye()){b&&z(b)}else return Ka(a,b,c)},kh=function(){return new Date},lh=function(){return f.location.href},mh=function(a){return ab(bb(a),"fragment")},nh=function(a){return $a(bb(a))},oh=function(a,b){return fd(a,b||2)},ph=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return Me(a)},qh=function(a,b){f[a]=b},L=function(a,b,c){b&&(void 0===f[a]||
c&&!f[a])&&(f[a]=b);return f[a]},rh=function(a,b,c){return cb(a,b,void 0===c?!0:!!c)},sh=function(a,b,c,d){var e={prefix:a,path:b,domain:c,Jd:d},g=sg();tg(g,e);zg(e)},th=function(a,b,c,d,e){var g=dg(),h=Vf();h.data||(h.data={query:{},fragment:{}},g(h.data));var k={},l=h.data;l&&(ya(k,l.query),ya(k,l.fragment));for(var m=qg(b),n=0;n<a.length;++n){var p=a[n];if(void 0!==pg[p]){var t=ug(p,m),q=k[t];if(q){var r=Math.min(vg(q),ua()),v;b:{for(var x=r,y=cb(t,u.cookie),
w=0;w<y.length;++w)if(vg(y[w])>x){v=!0;break b}v=!1}v||kb(t,q,c,d,0==e?void 0:new Date(r+1E3*(null==e?7776E3:e)),!0)}}}var B={prefix:b,path:c,domain:d};tg(rg(k.gclid,k.gclsrc),B);},uh=function(a,b,c,d,e){xg(a,b,c,d,e);},vh=function(a,b){if(ye()){b&&z(b)}else Ma(a,b)},wh=function(a){return!!Af(a,
"init",!1)},xh=function(a){yf(a,"init",!0)},yh=function(a,b,c){var d=(void 0===c?0:c)?"www.googletagmanager.com/gtag/js":vc;d+="?id="+encodeURIComponent(a)+"&l=dataLayer";b&&pa(b,function(e,g){g&&(d+="&"+e+"="+encodeURIComponent(g))});G(E("https://","http://",d))};
var Ah=tf.Rf;var Bh=new va;function Ch(a,b){function c(h){var k=bb(h),l=ab(k,"protocol"),m=ab(k,"host",!0),n=ab(k,"port"),p=ab(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,p]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0}
function Dh(a){var b=a.arg0,c=a.arg1;if(a.any_of&&ka(c)){for(var d=0;d<c.length;d++)if(Dh({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(v){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-
l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");return 0<=la(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var p;var t=a.ignore_case?"i":void 0;try{var q=String(c)+t,r=Bh.get(q);r||(r=new RegExp(c,t),Bh.set(q,r));p=r.test(b)}catch(v){p=!1}return p;case "_sw":return 0==String(b).indexOf(String(c));
case "_um":return Ch(b,c)}return!1};var Fh=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var Gh={},Hh=encodeURI,M=encodeURIComponent,Ih=Na;var Jh=function(a,b){if(!a)return!1;var c=ab(bb(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var Kh=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};Gh.Of=function(){var a=!1;return a};var ti=function(a,b,c,d){this.n=a;this.t=b;this.p=c;this.d=d},ui=function(){this.c=1;this.e=[];this.p=null};function vi(a){var b=sc,c=b.gss=b.gss||{};return c[a]=c[a]||new ui}var wi=function(a,b){vi(a).p=b},xi=function(a){var b=vi(a),c=b.p;if(c){var d=b.e,e=[];b.e=[];var g=function(h){for(var k=0;k<h.length;k++)try{var l=h[k];l.d?(l.d=!1,e.push(l)):c(l.n,l.t,l.p)}catch(m){}};g(d);g(e)}};var zi=function(){var a=f.gaGlobal=f.gaGlobal||{};a.hid=a.hid||oa();return a.hid};var Oi=window,Pi=document,Qi=function(a){var b=Oi._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===Oi["ga-disable-"+a])return!0;try{var c=Oi.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=cb("AMP_TOKEN",Pi.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return Pi.getElementById("__gaOptOutExtension")?!0:!1};var Xi=function(a,b,c){Wi(a);var d=Math.floor(ua()/1E3);vi(a).e.push(new ti(b,d,c,void 0));xi(a)},Yi=function(a,b,c){Wi(a);var d=Math.floor(ua()/1E3);vi(a).e.push(new ti(b,d,c,!0))},Wi=function(a){if(1===vi(a).c){vi(a).c=2;var b=encodeURIComponent(a);Ka(("http:"!=f.location.protocol?"https:":"http:")+("//www.googletagmanager.com/gtag/js?id="+b+"&l=dataLayer&cx=c"))}},$i=function(a,b){},Zi=function(a,b){};var W={a:{}};
W.a.jsm=["customScripts"],function(){(function(a){W.__jsm=a;W.__jsm.g="jsm";W.__jsm.h=!0;W.__jsm.b=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=L("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();
W.a.sp=["google"],function(){(function(a){W.__sp=a;W.__sp.g="sp";W.__sp.h=!0;W.__sp.b=0})(function(a){var b=a.vtp_gtmOnFailure;Bd();G("//www.googleadservices.com/pagead/conversion_async.js",function(){var c=L("google_trackConversion");if(ha(c)){var d={};"DATA_LAYER"==a.vtp_customParamsFormat?d=a.vtp_dataLayerVariable:"USER_SPECIFIED"==a.vtp_customParamsFormat&&(d=Kh(a.vtp_customParams,"key","value"));var e={};a.vtp_enableDynamicRemarketing&&(a.vtp_eventName&&(d.event=a.vtp_eventName),a.vtp_eventValue&&
(e.value=a.vtp_eventValue),a.vtp_eventItems&&(e.items=a.vtp_eventItems));c({google_conversion_id:a.vtp_conversionId,google_conversion_label:a.vtp_conversionLabel,google_custom_params:d,google_gtag_event_data:e,google_remarketing_only:!0,onload_callback:a.vtp_gtmOnSuccess,google_gtm:Ig()})||b()}else b()},b)})}();W.a.c=["google"],function(){(function(a){W.__c=a;W.__c.g="c";W.__c.h=!0;W.__c.b=0})(function(a){return a.vtp_value})}();
W.a.e=["google"],function(){(function(a){W.__e=a;W.__e.g="e";W.__e.h=!0;W.__e.b=0})(function(){return yc})}();
W.a.f=["google"],function(){(function(a){W.__f=a;W.__f.g="f";W.__f.h=!0;W.__f.b=0})(function(a){var b=oh("gtm.referrer",1)||u.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?ab(bb(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):nh(String(b)):String(b)})}();
W.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=wf(c,"gtm.click");ph(d)}}(function(b){W.__cl=b;W.__cl.g="cl";W.__cl.h=!0;W.__cl.b=0})(function(b){if(!wh("cl")){var c=L("document");Oa(c,"click",a,!0);xh("cl")}z(b.vtp_gtmOnSuccess)})}();W.a.k=["google"],function(){(function(a){W.__k=a;W.__k.g="k";W.__k.h=!0;W.__k.b=0})(function(a){return rh(a.vtp_name,oh("gtm.cookie",1),!!a.vtp_decodeCookie)[0]})}();
W.a.r=["google"],function(){(function(a){W.__r=a;W.__r.g="r";W.__r.h=!0;W.__r.b=0})(function(a){return oa(a.vtp_min,a.vtp_max)})}();
W.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){W.__u=b;W.__u.g="u";W.__u.h=!0;W.__u.b=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:oh("gtm.url",1))||lh();var d=b[a("vtp_component")];if(!d||"URL"==d)return nh(String(c));var e=bb(String(c)),g;if("QUERY"==d&&b[a("vtp_multiQueryKeys")])a:{var h=b[a("vtp_queryKey")],k;k=ka(h)?h:String(h||"").replace(/\s+/g,"").split(",");for(var l=0;l<k.length;l++){var m=ab(e,"QUERY",void 0,void 0,
k[l]);if(null!=m){g=m;break a}}g=void 0}else g=ab(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,"QUERY"==d?b[a("vtp_queryKey")]:void 0);return g})}();W.a.v=["google"],function(){(function(a){W.__v=a;W.__v.g="v";W.__v.h=!0;W.__v.b=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=oh(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();

W.a.ua=["google"],function(){var a,b=function(c){var d={},e={},g={},h={},k={};if(c.vtp_gaSettings){var l=c.vtp_gaSettings;Fa(Kh(l.vtp_fieldsToSet,"fieldName","value"),e);Fa(Kh(l.vtp_contentGroup,"index","group"),g);Fa(Kh(l.vtp_dimension,"index","dimension"),h);Fa(Kh(l.vtp_metric,"index","metric"),k);c.vtp_gaSettings=null;l.vtp_fieldsToSet=void 0;l.vtp_contentGroup=void 0;l.vtp_dimension=void 0;l.vtp_metric=void 0;var m=Fa(l);c=Fa(c,m)}Fa(Kh(c.vtp_fieldsToSet,"fieldName","value"),e);Fa(Kh(c.vtp_contentGroup,
"index","group"),g);Fa(Kh(c.vtp_dimension,"index","dimension"),h);Fa(Kh(c.vtp_metric,"index","metric"),k);var n=Id(c.vtp_functionName);if(ha(n)){var p="",t="";c.vtp_setTrackerName&&"string"==typeof c.vtp_trackerName?""!==c.vtp_trackerName&&(t=c.vtp_trackerName,p=t+"."):(t="gtm"+Cc(),p=t+".");var q={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},r={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},v=function(K){var P=[].slice.call(arguments,0);P[0]=p+P[0];n.apply(window,P)},x=function(K,P){return void 0===P?P:K(P)},y=function(K,P){if(P)for(var ba in P)P.hasOwnProperty(ba)&&v("set",K+ba,P[ba])},w=function(){
var K=function(Wa,qb,Bb){if(!Ea(qb))return!1;for(var gb=wa(Object(qb),Bb,[]),pe=0;gb&&pe<gb.length;pe++)v(Wa,gb[pe]);return!!gb&&0<gb.length},P;c.vtp_useEcommerceDataLayer?P=oh("ecommerce",1):c.vtp_ecommerceMacroData&&(P=c.vtp_ecommerceMacroData.ecommerce);if(!Ea(P))return;P=Object(P);var ba=wa(e,"currencyCode",P.currencyCode);void 0!==ba&&v("set","&cu",ba);K("ec:addImpression",P,"impressions");if(K("ec:addPromo",P[P.promoClick?"promoClick":"promoView"],"promotions")&&P.promoClick){v("ec:setAction",
"promo_click",P.promoClick.actionField);return}for(var na="detail checkout checkout_option click add remove purchase refund".split(" "),X=0;X<na.length;X++){var Z=P[na[X]];if(Z){K("ec:addProduct",Z,"products");v("ec:setAction",na[X],Z.actionField);break}}},B=function(K,P,ba){var na=0;if(K)for(var X in K)if(K.hasOwnProperty(X)&&(ba&&q[X]||!ba&&void 0===q[X])){var Z=r[X]?ra(K[X]):K[X];"anonymizeIp"!=X||Z||(Z=void 0);P[X]=Z;na++}return na},
A={name:t};B(e,A,!0);n("create",c.vtp_trackingId||d.trackingId,A);v("set","&gtm",Ig(!0));c.vtp_enableRecaptcha&&v("require","recaptcha","recaptcha.js");(function(K,P){void 0!==c[P]&&v("set",K,c[P])})("nonInteraction","vtp_nonInteraction");y("contentGroup",g);y("dimension",h);y("metric",k);var C={};B(e,C,!1)&&v("set",C);var D;c.vtp_enableLinkId&&
v("require","linkid","linkid.js");v("set","hitCallback",function(){var K=e&&e.hitCallback;ha(K)&&K();c.vtp_gtmOnSuccess()});if("TRACK_EVENT"==c.vtp_trackType){c.vtp_enableEcommerce&&(v("require","ec","ec.js"),w());var H={hitType:"event",eventCategory:String(c.vtp_eventCategory||d.category),eventAction:String(c.vtp_eventAction||d.action),eventLabel:x(String,c.vtp_eventLabel||d.label),eventValue:x(qa,c.vtp_eventValue||d.value)};B(D,
H,!1);v("send",H);}else if("TRACK_SOCIAL"==c.vtp_trackType){}else if("TRACK_TRANSACTION"==c.vtp_trackType){
v("require","ecommerce","//www.google-analytics.com/plugins/ua/ecommerce.js");var R=function(K){return oh("transaction"+K,1)},V=R("Id");v("ecommerce:addTransaction",{id:V,affiliation:R("Affiliation"),revenue:R("Total"),shipping:R("Shipping"),tax:R("Tax")});for(var Q=R("Products")||[],S=0;S<Q.length;S++){var O=Q[S];v("ecommerce:addItem",{id:V,sku:O.sku,name:O.name,category:O.category,price:O.price,quantity:O.quantity})}v("ecommerce:send");}else if("TRACK_TIMING"==
c.vtp_trackType){}else if("DECORATE_LINK"==c.vtp_trackType){}else if("DECORATE_FORM"==c.vtp_trackType){}else if("TRACK_DATA"==c.vtp_trackType){}else{c.vtp_enableEcommerce&&(v("require","ec","ec.js"),w());if(c.vtp_doubleClick||"DISPLAY_FEATURES"==c.vtp_advertisingFeaturesType){var T=
"_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");v("require","displayfeatures",void 0,{cookieName:T})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==c.vtp_advertisingFeaturesType){var U="_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");v("require","adfeatures",{cookieName:U})}D?v("send","pageview",D):v("send","pageview");c.vtp_autoLinkDomains&&Jd(p,c.vtp_autoLinkDomains,!!c.vtp_useHashAutoLink,!!c.vtp_decorateFormsAutoLink);
}if(!a){var Y=c.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";c.vtp_useInternalVersion&&!c.vtp_useDebugVersion&&(Y="internal/"+Y);a=!0;G(E("https:","http:","//www.google-analytics.com/"+Y,e&&e.forceSSL),function(){var K=Gd();K&&K.loaded||c.vtp_gtmOnFailure();},c.vtp_gtmOnFailure)}}else z(c.vtp_gtmOnFailure)};W.__ua=b;W.__ua.g="ua";W.__ua.h=!0;W.__ua.b=0}();
W.a.jel=["google"],function(){(function(a){W.__jel=a;W.__jel.g="jel";W.__jel.h=!0;W.__jel.b=0})(function(a){if(!wh("jel")){var b=L("self"),c=b.onerror;b.onerror=function(d,e,g,h,k){c&&c(d,e,g,h,k);ph({event:"gtm.pageError","gtm.errorMessage":d,"gtm.errorUrl":e,"gtm.errorLineNumber":g});return!1};xh("jel")}z(a.vtp_gtmOnSuccess)})}();



W.a.aev=["google"],function(){var a=void 0,b="",c=0,d=void 0,e={ATTRIBUTE:"gtm.elementAttribute",CLASSES:"gtm.elementClasses",ELEMENT:"gtm.element",ID:"gtm.elementId",HISTORY_CHANGE_SOURCE:"gtm.historyChangeSource",HISTORY_NEW_STATE:"gtm.newHistoryState",HISTORY_NEW_URL_FRAGMENT:"gtm.newUrlFragment",HISTORY_OLD_STATE:"gtm.oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"gtm.oldUrlFragment",TARGET:"gtm.elementTarget"},g=function(m){var n=oh(e[m.vtp_varType],1);return void 0!==n?n:m.vtp_defaultValue},h=function(m,
n){if(!m)return!1;var p=l(lh()),t;t=ka(n.vtp_affiliatedDomains)?n.vtp_affiliatedDomains:String(n.vtp_affiliatedDomains||"").replace(/\s+/g,"").split(",");for(var q=[p],r=0;r<t.length;r++)if(t[r]instanceof RegExp){if(t[r].test(m))return!1}else{var v=t[r];if(0!=v.length){if(0<=l(m).indexOf(v))return!1;q.push(l(v))}}return!Jh(m,q)},k=/^https?:\/\//i,l=function(m){k.test(m)||(m="http://"+m);return ab(bb(m),"HOST",!0)};(function(m){W.__aev=m;W.__aev.g="aev";W.__aev.h=!0;W.__aev.b=0})(function(m){switch(m.vtp_varType){case "TAG_NAME":return oh("gtm.element",
1).tagName||m.vtp_defaultValue;case "TEXT":var n,p=oh("gtm.element",1),t=oh("event",1),q=Number(kh());a===p&&b===t&&c>q-250?n=d:(d=n=p?Sa(p):"",a=p,b=t);c=q;return n||m.vtp_defaultValue;case "URL":var r;a:{var v=String(oh("gtm.elementUrl",1)||m.vtp_defaultValue||""),x=bb(v);switch(m.vtp_component||"URL"){case "URL":r=v;break a;case "IS_OUTBOUND":r=h(v,m);break a;default:r=ab(x,m.vtp_component,m.vtp_stripWww,m.vtp_defaultPages,m.vtp_queryKey)}}return r;case "ATTRIBUTE":var y;if(void 0===m.vtp_attribute)y=
g(m);else{var w=oh("gtm.element",1);y=Qa(w,m.vtp_attribute)||m.vtp_defaultValue||""}return y;default:return g(m)}})}();

W.a.smm=["google"],function(){(function(a){W.__smm=a;W.__smm.g="smm";W.__smm.h=!0;W.__smm.b=0})(function(a){var b=a.vtp_input,c=Kh(a.vtp_map,"key","value")||{};return c.hasOwnProperty(b)?c[b]:a.vtp_defaultValue})}();




W.a.html=["customScripts"],function(){function a(d,e,g,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,g,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=u.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,Ja(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var p=
[];k.firstChild;)p.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,p,l,h)()}else d.insertBefore(k,null),l()}else g()}catch(t){z(h)}}}var b=function(d,e,g){Ad(function(){var h,k=sc;k.postscribe||(k.postscribe=ec);h=k.postscribe;var l={done:e},m=u.createElement("div");m.style.display="none";m.style.visibility="hidden";u.body.appendChild(m);try{h(m,d,l)}catch(n){z(g)}})};var c=function(d){if(u.body){var e=
d.vtp_gtmOnFailure,g=Ah(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.nc,k=g.I;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(u.body,Ta(h),k,e)()}else jh(function(){c(d)},
200)};W.__html=c;W.__html.g="html";W.__html.h=!0;W.__html.b=0}();


W.a.lcl=[],function(){function a(){var c=L("document"),d=0,e=function(g){var h=g.target;if(h&&3!==g.which&&(!g.timeStamp||g.timeStamp!==d)){d=g.timeStamp;h=Ua(h,["a","area"],100);if(!h)return g.returnValue;var k=g.defaultPrevented||!1===g.returnValue,l=Af("lcl",k?"nv.mwt":"mwt",0),m;m=k?Af("lcl","nv.ids",[]):Af("lcl","ids",[]);if(m.length){var n=wf(h,"gtm.linkClick",m);if(b(g,h,c)&&!k&&l&&h.href){var p=L((h.target||"_self").substring(1)),t=!0;if(ph(n,function(){t&&p&&(p.location.href=h.href)},l))t=
!1;else return g.preventDefault&&g.preventDefault(),g.returnValue=!1}else ph(n,function(){},l||2E3);return!0}}};Oa(c,"click",e,!1);Oa(c,"auxclick",e,!1)}function b(c,d,e){if(2===c.which||c.ctrlKey||c.shiftKey||c.altKey||c.metaKey)return!1;var g=d.href.indexOf("#"),h=d.target;if(h&&"_self"!==h&&"_parent"!==h&&"_top"!==h||0===g)return!1;if(0<g){var k=nh(d.href),l=nh(e.location);return k!==l}return!0}(function(c){W.__lcl=c;W.__lcl.g="lcl";W.__lcl.h=!0;W.__lcl.b=0})(function(c){var d=void 0===c.vtp_waitForTags?
!0:c.vtp_waitForTags,e=void 0===c.vtp_checkValidation?!0:c.vtp_checkValidation,g=Number(c.vtp_waitForTagsTimeout);if(!g||0>=g)g=2E3;var h=c.vtp_uniqueTriggerId||"0";if(d){var k=function(m){return Math.max(g,m)};zf("lcl","mwt",k,0);e||zf("lcl","nv.mwt",k,0)}var l=function(m){m.push(h);return m};zf("lcl","ids",l,[]);e||zf("lcl","nv.ids",l,[]);wh("lcl")||(a(),xh("lcl"));z(c.vtp_gtmOnSuccess)})}();
W.a.evl=["google"],function(){function a(g,h){this.element=g;this.uid=h}function b(){var g=Number(oh("gtm.start"))||0;return kh().getTime()-g}function c(g,h,k,l){function m(){if(!ah(g.target)){h.has(e.rb)||h.set(e.rb,""+b());h.has(e.Zb)||h.set(e.Zb,""+b());var p=0;h.has(e.tb)&&(p=Number(h.get(e.tb)));p+=100;h.set(e.tb,""+p);if(p>=k){var t=wf(g.target,"gtm.elementVisibility",[h.uid]),q=$g(g.target);t["gtm.visibleRatio"]=Math.round(1E3*q)/10;t["gtm.visibleTime"]=k;t["gtm.visibleFirstTime"]=Number(h.get(e.Zb));
t["gtm.visibleLastTime"]=Number(h.get(e.rb));ph(t);l()}}}if(!h.has(e.La)&&(0==k&&m(),!h.has(e.sa))){var n=L("self").setInterval(m,100);h.set(e.La,n)}}function d(g){g.has(e.La)&&(L("self").clearInterval(Number(g.get(e.La))),g.remove(e.La))}var e={La:"polling-id-",Zb:"first-on-screen-",rb:"recent-on-screen-",tb:"total-visible-time-",sa:"has-fired-"};a.prototype.has=function(g){return!!this.element.getAttribute("data-gtm-vis-"+g+this.uid)};a.prototype.get=function(g){return this.element.getAttribute("data-gtm-vis-"+
g+this.uid)};a.prototype.set=function(g,h){this.element.setAttribute("data-gtm-vis-"+g+this.uid,h)};a.prototype.remove=function(g){this.element.removeAttribute("data-gtm-vis-"+g+this.uid)};(function(g){W.__evl=g;W.__evl.g="evl";W.__evl.h=!0;W.__evl.b=0})(function(g){function h(){var y=!1,w=null;if("CSS"===l){try{w=Te(m)}catch(D){}y=!!w&&v.length!=w.length}else if("ID"===l){var B=Ra(m);B&&(w=[B],y=1!=v.length||v[0]!==B)}w||(w=[],y=0<v.length);if(y){for(var A=0;A<v.length;A++)d(new a(v[A],q));v=[];
for(var C=0;C<w.length;C++)v.push(w[C]);0<=x&&gh(x);0<v.length&&(x=fh(k,v,[t]))}}function k(y){var w=new a(y.target,q);y.intersectionRatio>=t?w.has(e.sa)||c(y,w,p,"ONCE"===r?function(){for(var B=0;B<v.length;B++){var A=new a(v[B],q);A.set(e.sa,"1");d(A)}gh(x);if(n&&Qg)for(var C=0;C<Qg.length;C++)Qg[C]===h&&Qg.splice(C,1)}:function(){w.set(e.sa,"1");d(w)}):(d(w),"MANY_PER_ELEMENT"===r&&w.has(e.sa)&&(w.remove(e.sa),w.remove(e.tb)),w.remove(e.rb))}var l=g.vtp_selectorType,m;"ID"===l?m=String(g.vtp_elementId):
"CSS"===l&&(m=String(g.vtp_elementSelector));var n=!!g.vtp_useDomChangeListener,p=g.vtp_useOnScreenDuration&&Number(g.vtp_onScreenDuration)||0,t=(Number(g.vtp_onScreenRatio)||50)/100,q=g.vtp_uniqueTriggerId,r=g.vtp_firingFrequency,v=[],x=-1;h();n&&Rg(h);z(g.vtp_gtmOnSuccess)})}();

var aj={};aj.macro=function(a){if(tf.$b.hasOwnProperty(a))return tf.$b[a]},aj.onHtmlSuccess=tf.td(!0),aj.onHtmlFailure=tf.td(!1);aj.dataLayer=gd;aj.callback=function(a){Ac.hasOwnProperty(a)&&ha(Ac[a])&&Ac[a]();delete Ac[a]};aj.Ve=function(){sc[rc.m]=aj;Bc=W.a;Rb=Rb||tf;Sb=sd};
aj.Jf=function(){sc=f.google_tag_manager=f.google_tag_manager||{};if(sc[rc.m]){var a=sc.zones;a&&a.unregisterChild(rc.m)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)Kb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)Nb.push(e[g]);for(var h=b.predicates||[],k=0;k<h.length;k++)Mb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],p={},t=0;t<
n.length;t++)p[n[t][0]]=Array.prototype.slice.call(n[t],1);Lb.push(p)}Pb=W;Qb=Dh;aj.Ve();Re();vd=!1;wd=0;if("interactive"==u.readyState&&!u.createEventObject||"complete"==u.readyState)yd();else{Oa(u,"DOMContentLoaded",yd);Oa(u,"readystatechange",yd);if(u.createEventObject&&u.documentElement.doScroll){var q=!0;try{q=!f.frameElement}catch(y){}q&&zd()}Oa(f,"load",yd)}Fe=!1;"complete"===u.readyState?He():Oa(f,"load",He);a:{if(!Xc)break a;f.setInterval(Yc,864E5);}xc=(new Date).getTime();}};(0,aj.Jf)();

})()
