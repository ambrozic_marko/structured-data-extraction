# Python core imports
import json
import re
# Own imports
from implementation.fileReader import FileReader

# Third party library imports
from lxml import etree
from lxml import html


def cleanHtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def printItemsAsJSON(items):
    result = []
    for item in items:
        result.append(item)
    print(json.dumps(result))


class OverstockReader:
    base_path = "input/overstock.com"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []

        root = etree.fromstring(self.content, html.HTMLParser())

        elements = root.xpath("//body/table[2]/tbody/tr/td[5]/table/tbody/tr[2]//table//table/tbody/tr[@bgcolor]/td[@valign]/a/b")
        titles = []
        for item_row in elements:
            titles.append(item_row.text)

        elements = root.xpath("//body/table[2]/tbody/tr/td[5]/table/tbody/tr[2]//table//table/tbody/tr[@bgcolor]/td[@valign]/table/tbody/tr/td/span[@class='normal']")
        contents = []
        for item_row in elements:
            contents.append(item_row.text)

        elements = root.xpath("//body/table[2]/tbody/tr/td[5]/table/tbody/tr[2]//table//table/tbody/tr[@bgcolor]/td[@valign]/table/tbody/tr/td/table/tbody/tr[1]//s")
        list_prices = []
        for item_row in elements:
            list_prices.append(item_row.text)

        elements = root.xpath("//body/table[2]/tbody/tr/td[5]/table/tbody/tr[2]//table//table/tbody/tr[@bgcolor]/td[@valign]/table/tbody/tr/td/table/tbody/tr[2]//span/b")
        prices = []
        for item_row in elements:
            prices.append(item_row.text)

        elements = root.xpath("//body/table[2]/tbody/tr/td[5]/table/tbody/tr[2]//table//table/tbody/tr[@bgcolor]/td[@valign]/table/tbody/tr/td/table/tbody/tr[3]//span[@class='littleorange']")
        savings = []
        for item_row in elements:
            savings.append(item_row.text)

        for i in range(len(titles)):
            item = {
                "Title": titles[i],
                "ListPrice": list_prices[i],
                "Price": prices[i],
                "Saving": self.parse_savings(savings[i]),
                "SavingPercent": self.parse_savings(savings[i], False),
                "Content": contents[i]
            }
            items.append(item)

        return items

    def parse_savings(self, saving, value=True):
        if value:
            return saving.split()[0]
        else:
            return saving.split()[1][1:-1]

    def print_content(self):
        print(self.content)


class RtvsloReader:
    base_path = "input/rtvslo.si"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")

    def parse_content(self):
        root = etree.fromstring(self.content, html.HTMLParser())

        title = root.xpath("//h1")[0].text
        subtitle = root.xpath("//div[@class='subtitle']")[0].text
        lead = root.xpath("//p[@class='lead']")[0].text
        content = cleanHtml(etree.tostring(root.xpath("//div[@class='article-body']")[0]).decode("utf-8", errors='ignore'))
        author = root.xpath("//div[@class='author-name']")[0].text

        date = etree.tostring(root.xpath("//div[@class='publish-meta']")[0]).decode("utf-8", errors='ignore')
        regexExpr = "(?<=>).*(?=<br)"
        date = cleanHtml(re.search(regexExpr, date).group(0))

        item_dict = {
            "Author": author,
            "PublishTime": date,
            "Title": title,
            "Subtitle": subtitle,
            "Lead": lead,
            "Content": content
        }

        return item_dict

    def print_content(self):
        print(self.content)


class TopshopReader:
    base_path = "input/topshop.si"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []

        root = etree.fromstring(self.content, html.HTMLParser())
        elements = root.xpath("//div[@id='catalog-listing']//h2")
        titles = []
        for item_row in elements:
            titles.append(item_row.text.strip())

        elements = root.xpath("//div[@id='catalog-listing']//p[@class='old-price']/span[@class='price']")
        normal_prices = []
        for item_row in elements:
            normal_prices.append(item_row.text.strip().replace("\xa0€", " "))

        elements = root.xpath("//div[@id='catalog-listing']//p[@class='special-price']/span[@class='price']")
        special_prices = []
        for item_row in elements:
            special_prices.append(item_row.text.strip().replace("\xa0€", " "))

        elements = root.xpath("//div[@id='catalog-listing']//div[@class='list-image-box']/div/span")
        discounts = []
        for item_row in elements:
            discounts.append(item_row.text.strip().replace("\xa0€", " "))

        for i in range(len(titles)):
            item = {
                "Name": titles[i],
                "NormalPrice": normal_prices[i],
                "SpecialPrice": special_prices[i],
                "DiscountPercent": discounts[i]
            }
            items.append(item)

        return items

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")
        self.content = ' '.join(self.content.split())

    def print_content(self):
        print(self.content)


class RedditReader:
    base_path = "input/reddit.com"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []
        root = etree.fromstring(self.content, html.HTMLParser())

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//p[@class='title']/a")
        titles = []
        for item_row in elements:
            titles.append(item_row.text.strip())

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//time")
        times = []
        for item_row in elements:
            times.append(item_row.text.strip())

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//p[@class='tagline ']/a")
        users = []
        for item_row in elements:
            users.append(item_row.text.strip())

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//a[@data-event-action='thumbnail']")
        thumbnails = []
        for item_row in elements:
            thumbnails.append(item_row.attrib["href"])

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//a[@data-event-action='title']")
        urls = []
        for item_row in elements:
            urls.append(item_row.attrib["href"])

        elements = root.xpath("//div[@id='siteTable']//div[@data-context='listing']//div[@class='score unvoted']")
        likes = []
        for item_row in elements:
            likes.append(item_row.text)

        for i in range(len(titles)):
            item = {
                "Title": titles[i],
                "Time": times[i],
                "User": users[i],
                "ThumbnailUrl": thumbnails[i],
                "PostUrl": urls[i],
                "Likes": likes[i]

            }
            items.append(item)

        return items

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")
        self.content = ' '.join(self.content.split())

    def print_content(self):
        print(self.content)


overstock_reader = OverstockReader("jewelry01.html")
overstock_reader.read_content()
printItemsAsJSON(overstock_reader.get_items())


overstock_reader = OverstockReader("jewelry02.html")
overstock_reader.read_content()
printItemsAsJSON(overstock_reader.get_items())


rtvslo_reader = RtvsloReader("Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html")
rtvslo_reader.read_content()
print(json.dumps(rtvslo_reader.parse_content()))

rtvslo_reader = RtvsloReader("Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html")
rtvslo_reader.read_content()
print(json.dumps(rtvslo_reader.parse_content()))

topshop_reader = TopshopReader("Akcija.html")
topshop_reader.read_content()
printItemsAsJSON(topshop_reader.get_items())

topshop_reader = TopshopReader("Dormeo-Akcija.html")
topshop_reader.read_content()
printItemsAsJSON(topshop_reader.get_items())

