# Python core imports
import re
import json
# Third party library imports

# Own imports
from implementation.fileReader import FileReader


def cleanHtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def printItemsAsJSON(items):
    result = []
    for item in items:
        result.append(item)
    print(json.dumps(result))


class OverstockReader:
    base_path = "input/overstock.com"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []
        # Find table row containing similar list items
        pattern = "<tr bgcolor=\"[#a-f0-9]+\"> <td valign=\"top\" align=\"center\">(.*?)ms.gif\" width=\"1\" height=\"4\" border=\"0\"><\\/td><\\/tr>"
        for item in re.findall(pattern, self.content, re.IGNORECASE):
            overstock_item = OverstockItem()
            overstock_item.html_content = item
            if overstock_item.matches_criteria():
                items.append(overstock_item.get_item_dict())

        return items

    def print_content(self):
        print(self.content)


class OverstockItem:
    html_content = ""

    def matches_criteria(self):
        patterns = [
            "<img (.*?)>",
            "<a (.*?)>",
            "PROD_ID=[0-9]+"
        ]

        for pattern in patterns:
            if re.search(pattern, self.html_content) is None:
                return False

        return True

    def get_title(self):
        title_pattern = "(?<=<b>)(.+?)(?=<\\/b><\\/a>)"
        return re.search(title_pattern, self.html_content).group(0)

    def get_price_value_by_label(self, label):
        price_row_pattern = "<tr><td align=\"right\" nowrap=\"nowrap\"><b>" + label + "<\\/b>(.+?)<\\/tr>"
        price_row_html = re.search(price_row_pattern, self.html_content).group(0)
        price_pattern = "\\$([0-9]{1,3},)*[0-9]{1,10}\\.[0-9]{2}( \\([0-9]{1,2}%\\))*"

        price_match = re.search(price_pattern, price_row_html)
        if price_match:
            return price_match.group(0)
        return None

    def get_saving_percent(self):
        saving_text = self.get_price_value_by_label("You Save:")
        saving_pattern = "[0-9]{1,2}%"
        if saving_text:
            return re.search(saving_pattern, saving_text).group(0)
        return None

    def get_saving_value(self):
        saving_text = self.get_price_value_by_label("You Save:")
        saving_pattern = "\\$([0-9]{1,3},)*[0-9]{1,10}\\.[0-9]{2}"
        if saving_text:
            return re.search(saving_pattern, saving_text).group(0)
        return None

    def get_content(self):
        content_pattern = "(?<=<td valign=\"top\"><span class=\"normal\">)(.*?)(?=<\\/span>)"
        return re.search(content_pattern, self.html_content).group(0)

    def get_item_dict(self):
        return {
            "Title": self.get_title(),
            "ListPrice": self.get_price_value_by_label("List Price:"),
            "Price": self.get_price_value_by_label("Price:"),
            "Saving": self.get_saving_value(),
            "SavingPercent": self.get_saving_percent(),
            "Content": self.get_content()
        }


class RtvsloReader:
    base_path = "input/rtvslo.si"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")

    def parse_content(self):
        item = RtvsloItem()
        item.html_content = self.content

        item_dict = {
            "Author": item.get_author(),
            "PublishTime": item.get_publish_time(),
            "Title": item.get_title(),
            "Subtitle": item.get_subtitle(),
            "Lead": item.get_lead(),
            "Content": item.get_content()
        }

        return item_dict

    def print_content(self):
        print(self.content)


class RtvsloItem:
    html_content = ""

    def get_author(self):
        return self.get_element_by_tag_and_class("div", "author-name")

    def get_publish_time(self):
        public_time_pattern = ".+(?=<br>)"
        return re.search(public_time_pattern, self.get_element_by_tag_and_class("div", "publish-meta")).group(0)

    def get_title(self):
        title_pattern = "(?<=<h1>).+(?=</h1>)"
        return re.search(title_pattern, self.html_content).group(0)

    def get_subtitle(self):
        return self.get_element_by_tag_and_class("div", "subtitle")

    def get_lead(self):
        return self.get_element_by_tag_and_class("p", "lead")

    def get_content(self):
        content_pattern = "(?<=<div class=\"article-body\">)(.+?)(?=<\\/article><\\/div>)"
        return re.search(content_pattern, self.html_content).group(0)

    def get_element_by_tag_and_class(self, tag, class_name):
        pattern = "(?<=<" + tag + " class=\"" + class_name + "\">)(.+?)(?=<\\/" + tag + ">)"
        return re.search(pattern, self.html_content).group(0)


class TopshopReader:
    base_path = "input/topshop.si"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []
        pattern = "<div class=\"li-content\">(.*?)<\\/li>"
        for item in re.findall(pattern, self.content, re.IGNORECASE):
            ts_item = TopshopItem()
            ts_item.html_content = item
            items.append(ts_item.get_item_dict())
        return items

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")
        self.content = ' '.join(self.content.split())

    def print_content(self):
        print(self.content)


class TopshopItem:
    html_content = ""

    def print_item(self):
        print(self.get_item_dict())

    def get_item_dict(self):
        item = {
            "Name": self.get_item_name(),
            "NormalPrice": self.get_item_normal_price(),
            "SpecialPrice": self.get_item_special_price(),
            "DiscountPercent": self.extract_percentage_discount()
        }
        return item

    def get_item_name(self):
        return cleanHtml(self.get_element_by_tag_and_class("h2", "product-name")).strip()

    def get_item_special_price(self):
        return cleanHtml(self.extract_price(self.get_element_by_tag_and_class("p", "special-price")))

    def get_item_normal_price(self):
        return cleanHtml(self.extract_price(self.get_element_by_tag_and_class("p", "old-price")))

    def get_element_by_tag_and_class(self, tag, class_name):
        pattern = '<'+tag+' class="'+class_name+'(.*?)>(.*?)<\\/'+tag+'>'
        return re.search(pattern, self.html_content).group(0)

    def extract_price(self, html):
        price_pattern = '[0-9]{1,5},[0-9]{2}'
        return re.search(price_pattern, html).group(0)

    def extract_percentage_discount(self):
        discount_pattern = '-[0-9]{1,2}%'
        return re.search(discount_pattern, self.html_content).group(0)


class RedditReader:
    base_path = "input/reddit.com"
    content = ""
    page = ""

    def __init__(self, page):
        self.page = page

    def get_items(self):
        return self.parse_items()

    def parse_items(self):
        items = []
        pattern = "<div class=\" *thing (.*?)>(.*?)<div class=\"clearleft\">"
        for item in re.finditer(pattern, self.content, re.IGNORECASE):
            rd_item = RedditItem()
            rd_item.html_content = item.group(0)
            items.append(rd_item.get_item_dict())
        return items

    def read_content(self):
        self.content = FileReader.getContent(self.base_path + "/" + self.page)
        self.content = self.content.replace("\n", "").replace("\r", "").replace("\t", "")
        self.content = ' '.join(self.content.split())

    def print_content(self):
        print(self.content)


class RedditItem:
    html_content = ""

    def print_item(self):
        print(self.html_content)

    def get_item_dict(self):
        item = {
            "Title": cleanHtml(self.get_element_by_tag_and_class("a", "title")),
            "Time": cleanHtml(self.get_time()),
            "User": cleanHtml(self.get_author()),
            "ThumbnailUrl": self.get_thumbnail_url(),
            "PostUrl": self.get_post_url(),
            "Likes": cleanHtml(self.get_element_by_tag_and_class("div", "score unvoted"))

        }
        return item

    def get_time(self):
        pattern = '<time .*?>(.*?)<\\/time>'
        match = re.search(pattern, self.html_content)
        if match is not None:
            return match.group(0)
        return ""

    def get_author(self):
        pattern = '(?<=by )<a href.*?class="author(.*?)">(.*?)<\\/a>'
        match = re.search(pattern, self.html_content)
        if match is not None:
            return match.group(0)
        return ""

    def get_thumbnail_url(self):
        pattern = '(?<="thumbnail" href=")(.*?)(?=")'
        match = re.search(pattern, self.html_content)
        if match is not None:
            return match.group(0)
        return ""

    def get_post_url(self):
        pattern = '(?<="title" href=")(.*?)(?=")'
        match = re.search(pattern, self.html_content)
        if match is not None:
            return match.group(0)
        return ""

    def get_element_by_tag_and_class(self, tag, class_name):
        pattern = '<'+tag+' class="'+class_name+'(.*?)>(.*?)<\\/'+tag+'>'
        match = re.search(pattern, self.html_content)
        if match is not None:
            return match.group(0)
        return ""


overstock_reader = OverstockReader("jewelry01.html")
overstock_reader.read_content()
printItemsAsJSON(overstock_reader.get_items())

overstock_reader = OverstockReader("jewelry02.html")
overstock_reader.read_content()
printItemsAsJSON(overstock_reader.get_items())

rtvslo_reader = RtvsloReader("Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html")
rtvslo_reader.read_content()
print(json.dumps(rtvslo_reader.parse_content()))

rtvslo_reader = RtvsloReader("Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html")
rtvslo_reader.read_content()
print(json.dumps(rtvslo_reader.parse_content()))

topshop_reader = TopshopReader("Akcija.html")
topshop_reader.read_content()
printItemsAsJSON(topshop_reader.get_items())

topshop_reader = TopshopReader("Dormeo-Akcija.html")
topshop_reader.read_content()
printItemsAsJSON(topshop_reader.get_items())

