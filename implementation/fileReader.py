class FileReader:
    @staticmethod
    def getContentLines(filename):
        pageUrls = []
        with open(filename) as file:
            line = file.readline()
            while line:
                pageUrls.append(line.replace("\n", ""))
                line = file.readline()
        return pageUrls

    @staticmethod
    def getContent(filename):
        content = ""
        with open(filename, 'rb') as file:
            line = file.readline()
            while line:
                content = content + line.decode("utf-8", "ignore")
                line = file.readline()
        return content

